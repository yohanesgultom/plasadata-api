module.exports = function(Balancehistory) {

    Balancehistory.STATUS = {
        TOPUP: 'TOPUP',
        PURCHASE: 'PURCHASE',
        REFUND: 'REFUND',
        GIFT: 'GIFT'
    }
};
