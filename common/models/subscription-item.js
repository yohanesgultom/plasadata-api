module.exports = function(Subscriptionitem) {

    Subscriptionitem.prototype.getExcerpt = function(customerId) {
        var excerpt = {
            id: this.id,
            name: this.name,
            publishDate: this.publishDate,
            file: this.file
        }
        if (customerId) {
            excerpt.relativeUrl = '/api/Customers/' + customerId + '/subscriptionItems/' + this.id + '/download'
        }
        return excerpt
    }

};
