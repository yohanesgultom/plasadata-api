module.exports = function(Message) {

    var app = require('../../server/server'),
        moment = require('moment')

    Message.createSubscriptionMessages = function (date, cb) {
        date = date ? date : new Date()
        var start = moment(date).startOf('day'),
            end = moment(date).endOf('day')

        app.models.SubscriptionItem.find({
            where: {publishDate: {between: [start, end]}}
        }, function (err, items) {
            if (err) {
                cb(err)
            } else {
                var productIds = []
                items.forEach(function (i) {
                    if (productIds.indexOf(i.productId) < 0) {
                        productIds.push(i.productId)
                    }
                })
                app.models.Purchase.find({where: {productId: {inq: productIds}}, include: 'product'}, function (err, purchases) {
                    if (err) {
                        cb(err)
                    } else {
                        var messages = []
                        purchases.forEach(function (purchase) {
                            var product = purchase.product(),
                                attachments = getItemExceptsBySubscriptionId(purchase.productId, purchase.customerId, items)
                            if (attachments && attachments.length > 0) {
                                messages.push({
                                    customerId: purchase.customerId,
                                    subject: purchase.product().name + ' ' + start.format('dddd, D MMMM YYYY'),
                                    body: 'Hi, this is your new item(s) for today',
                                    attachments: attachments,
                                    tags: product.tags
                                })
                            }
                        })
                        if (messages.length > 0) {
                            Message.create(messages, cb)
                        } else {
                            cb(null, null)
                        }
                    }
                })
            }
        })
    }

    function getItemExceptsBySubscriptionId(subscriptionId, customerId, items) {
        var result = []
        for (var i = 0; i < items.length; i++) {
            if (items[i].productId == subscriptionId) {
                result.push(items[i].getExcerpt(customerId))
            }
        }
        return result
    }

};
