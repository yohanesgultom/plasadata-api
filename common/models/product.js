var moment = require('moment')

module.exports = function(Product) {

    var app = require('../../server/server')

    Product.prototype.setPrice = function() {
        var product = this
        product.price = product.basePrice
        return new Promise(function(resolve, reject) {
            product.promotions(function(err, promotions) {
                if (err) {
                    reject(err)
                } else {
                    // add dynamic field
                    // to store excerpts of promotions
                    // to be used during purchase
                    product.promoExcerpts = []
                    if (promotions) {
                        promotions.forEach(function(promo){
                            product.promoExcerpts.push(promo.getExcerpt())
                            // make sure it's 2 decimals
                            product.price = Math.round((product.price - (product.price * promo.discount / 100.0)) * 100) / 100
                        })
                    }
                    resolve(product)
                }
            })
        })
    }

    Product.prototype.getExcerpt = function(customerId) {
        var excerpt = {
            id: this.id,
            name: this.name,
            file: this.file,
            tags: this.tags
        }
        // download URL relative to API path
        if (customerId) {
            excerpt.relativeUrl = '/api/Customers/' + customerId + '/products/ ' + this.id + '/download'
        }
        // subscription
        if (this.startDate && this.endDate) {
            excerpt.startDate = this.startDate
            excerpt.endDate = this.endDate
        }
        return excerpt
    }

    /***************** Disable REST API *****************/

    // Product.disableRemoteMethod("find", true)
    // Product.disableRemoteMethod("findOne", true)
    Product.disableRemoteMethod("create", true)
    Product.disableRemoteMethod("upsert", true)
    Product.disableRemoteMethod("updateAll", true)
    Product.disableRemoteMethod("deleteById", true)
    Product.disableRemoteMethod("replaceOrCreate", true)
    // Product.disableRemoteMethod("count", true)
    // Product.disableRemoteMethod("exists", true)
    Product.disableRemoteMethod("createChangeStream", true)
    Product.disableRemoteMethod("updateAttributes", false)
    Product.disableRemoteMethod("replaceById", false)
    Product.disableRemoteMethod("upsertWithWhere", false)

    // Product.disableRemoteMethod('__get__customers', false)
    Product.disableRemoteMethod('__create__customers', false)
    Product.disableRemoteMethod('__upsert__customers', false)
    Product.disableRemoteMethod('__delete__customers', false)
    Product.disableRemoteMethod('__updateById__customers', false)
    Product.disableRemoteMethod('__destroyById__customers', false)
    Product.disableRemoteMethod('__link__customers', false)
    Product.disableRemoteMethod('__unlink__customers', false)

    Product.disableRemoteMethod('__create__promotions', false)
    Product.disableRemoteMethod('__upsert__promotions', false)
    Product.disableRemoteMethod('__delete__promotions', false)
    Product.disableRemoteMethod('__updateById__promotions', false)
    Product.disableRemoteMethod('__destroyById__promotions', false)
    Product.disableRemoteMethod('__link__promotions', false)
    Product.disableRemoteMethod('__unlink__promotions', false)

    Product.disableRemoteMethod('__create__wishers', false)
    Product.disableRemoteMethod('__upsert__wishers', false)
    Product.disableRemoteMethod('__delete__wishers', false)
    Product.disableRemoteMethod('__updateById__wishers', false)
    Product.disableRemoteMethod('__destroyById__wishers', false)
    Product.disableRemoteMethod('__link__wishers', false)
    Product.disableRemoteMethod('__unlink__wishers', false)

    Product.disableRemoteMethod('__create__bundles', false)
    Product.disableRemoteMethod('__upsert__bundles', false)
    Product.disableRemoteMethod('__delete__bundles', false)
    Product.disableRemoteMethod('__updateById__bundles', false)
    Product.disableRemoteMethod('__destroyById__bundles', false)
    Product.disableRemoteMethod('__link__bundles', false)
    Product.disableRemoteMethod('__unlink__bundles', false)

    Product.disableRemoteMethod('__create__subscriptionItems', false)
    Product.disableRemoteMethod('__upsert__subscriptionItems', false)
    Product.disableRemoteMethod('__delete__subscriptionItems', false)
    Product.disableRemoteMethod('__updateById__subscriptionItems', false)
    Product.disableRemoteMethod('__destroyById__subscriptionItems', false)

    Product.disableRemoteMethod('__create__albums', false)
    Product.disableRemoteMethod('__upsert__albums', false)
    Product.disableRemoteMethod('__delete__albums', false)
    Product.disableRemoteMethod('__updateById__albums', false)
    Product.disableRemoteMethod('__destroyById__albums', false)
    Product.disableRemoteMethod('__link__albums', false)
    Product.disableRemoteMethod('__unlink__albums', false)

    /***************** Hooks *****************/

    // force excluding expired and future promotions
    Product.observe('access', function(ctx, next) {
        var now = new Date(),
            defaultInclude = {
                promotions: {
                    relation: 'promotions',
                    scope: {
                        where: {
                            startDate: {lt: now},
                            endDate: {gt: now}
                        }
                    }
                },
                albums: {
                    relation: 'albums'
                }
            }

        // set default include if not defined
        if (!ctx.query.include) {
            ctx.query.include = []
            for (var key in defaultInclude) {
                ctx.query.include.push(defaultInclude[key])
            }
        }

        next()
    })

    Product.afterRemote('find', function(ctx, modelInstance, next) {
        Product.setPrice(ctx, function() {
            next()
        })
    })

    Product.afterRemote('findById', function(ctx, modelInstance, next) {
        Product.setPrice(ctx, function() {
            next()
        })
    })

    // include bundles automatically
    // Product.beforeRemote('findById', function(ctx, modelInstance, next) {
    //     var filter = ctx.args.filter ? JSON.parse(ctx.args.filter) : {}
    //     if (filter.include && filter.include.constructor === Array) {
    //         filter.include.push('bundles')
    //     } else if (filter.include) {
    //         filter.include = [filter.include, 'bundles']
    //     } else {
    //         filter = {include: 'bundles'}
    //     }
    //     ctx.args.filter = JSON.stringify(filter)
    //     next()
    // })

    /***************** REST API *****************/

    Product.setPrice = function (ctx, cb) {
        if (ctx.result) {
            var promises = [],
                products = ctx.result.constructor === Array ? ctx.result : [ctx.result]

            products.forEach(function(product){
                promises.push(product.setPrice())
            })
            Promise.all(promises).then(function(values){
                cb()
            }, function(err){
                cb(err, null)
            })
        }
    }

    // get tags
    Product.getTags = function (limit = 0, offset = 0, ctx, cb) {
        var productCollection = Product.getDataSource().connector.collection(Product.modelName),
            params = [
                { "$project": { "tags": 1, "tags":{$slice: ["$tags", 1]} }},
                { "$unwind": "$tags" },
                { "$group": { "_id": "$tags", "count": { "$sum": 1 }}},
                { "$sort": {"count": -1}}
            ]

        if (offset > 0) {
            params.push({ "$skip": offset })
        }
        if (limit > 0) {
            params.push({ "$limit": limit })
        }

        productCollection.aggregate(params, function(err, tags) {
            tags = tags.map(tag => {
                return tag._id
            })
            cb(err, tags)
        })
    }

    // get sub tags
    Product.getSubTags = function (tag, limit = 0, offset = 0, ctx, cb) {
        var productCollection = Product.getDataSource().connector.collection(Product.modelName),
            params = [
                { "$match": {"tags": tag} },
                { "$project": { "tags": 1 }},
                { "$limit": 1 }
            ]
            pos = 1

        // get tag tier level
        productCollection.aggregate(params, function(err, obj) {
            if (err) {
                cb(err, null)
            } else if (!obj || obj.length <= 0 || !obj[0]) {
                cb(null, [])
            } else {
                var pos = Math.abs(obj[0].tags.indexOf(tag)) + 1
                params = [
                        { "$match": {"tags": tag} },
                        { "$project": { "tags": 1, "tags": {$slice: ["$tags", pos, 1]} }},
                        { "$unwind": "$tags" },
                        { "$group": { "_id": "$tags", "count": { "$sum": 1 }}},
                        { "$sort": {"count": -1}}
                    ]

                if (offset > 0) {
                    params.push({ "$skip": offset })
                }
                if (limit > 0) {
                    params.push({ "$limit": limit })
                }

                productCollection.aggregate(params, function(err, tags) {
                    tags = tags.map(tag => {
                        return tag._id
                    })
                    cb(err, tags)
                })
            }

        })

    }

    // top selling products
    Product.getTopSellingProducts = function (tag, limit = 50, offset = 0, ctx, cb) {
        var sql = 'select productid, count(id) from purchase',
            params = [],
            count = 1

        // where
        if (tag) {
            params.push('%"' + tag + '"%')
            sql += ' where productTags like $' + count
            count++
        }
        sql += ' group by productid'

        // limit and offset
        if (limit > 0) {
            params.push(limit)
            sql += ' limit $' + count
            count++
        }
        if (offset > 0) {
            params.push(offset)
            sql += ' offset $' + count
            count++
        }

        // console.log(sql, params)
        app.models.Purchase.getDataSource().connector.execute(sql, params, function (err, res) {
            if (res && res.length > 0) {
                var productIds = res.map(function(r) {return r.productid})
                Product.find({where: {id: {inq: productIds}}}, cb)
            } else {
                cb(err, [])
            }
        })
    }

    // get promoted products
    Product.getPromotedProducts = function (tag, limit = 50, offset = 0, ctx, cb) {
        var filter = {include: {relation: 'products', scope: {include: ['promotions']}}}
        if (tag) {
            filter.include.scope.where = {tags: {inq: [tag]}}
        }
        app.models.Promotion.find(filter, function (err, promotions) {
            if (err) {
                cb(err, null)
            } else {
                var productIds = [],
                    products = []
                promotions.forEach(function (promo) {
                    promo.products().forEach(function (prod) {
                        var index = productIds.indexOf(prod.id)
                        if (index < 0) {
                            productIds.push(prod.id)
                            products.push(prod)
                        }
                    })
                })
                var end = offset + limit
                end = end <= products.length ? end : -1
                cb(null, products.slice(offset, end))
            }
        })
    }

    // count promoted products
    Product.countPromotedProducts = function (tag, ctx, cb) {
        var filter = {include: {relation: 'products'}}
        if (tag) {
            filter.include.scope = {where: {tags: {inq: [tag]}}}
        }
        app.models.Promotion.find(filter, function (err, promotions) {
            if (err) {
                cb(err, null)
            } else {
                var productMap = {}
                promotions.forEach(function (promo) {
                    promo.products().forEach(function (prod) {
                        productMap[prod.id] = prod.id
                    })
                })
                cb(null, {count: Object.keys(productMap).length})
            }
        })
    }

    Product.downloadSample = function (id, ctx, cb) {
        Product.findById(id, function (err, product) {
            if (err) {
                cb(err, null)
            } else if (!product || !product.sample) {
                err = new Error()
                err.status = 404
                err.message = 'Not found'
                cb(err, null)
            } else {
                app.models.contentcontainer.download('samples', product.sample, ctx.req, ctx.res, cb)
            }
        })
    }

    Product.remoteMethod(
        'getTags',
        {
            description: 'Get available product tags ordered desc from the most',
            accepts: [
                { arg: 'limit', type: 'number', description: 'Limit of tags' },
                { arg: 'offset', type: 'number', description: 'Offset of tags' },
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: {
                arg: 'tags', type: ['String'], root: true, description: 'List of tags'
            },
            http: {path: '/tags', verb: 'get'}
        }
    )

    Product.remoteMethod(
        'getSubTags',
        {
            description: 'Get available product sub tags ordered desc from the most',
            accepts: [
                { arg: 'tag', type: 'string', description: 'Parent tag', required: true, 'default': 'music' },
                { arg: 'limit', type: 'number', description: 'Limit of tags' },
                { arg: 'offset', type: 'number', description: 'Offset of tags' },
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: {
                arg: 'tags', type: ['String'], root: true, description: 'List of tags'
            },
            http: {path: '/tags/:tag', verb: 'get'}
        }
    )

    Product.remoteMethod(
        'getTopSellingProducts',
        {
            description: 'Get top selling products',
            accepts: [
                { arg: 'tag', type: 'string', description: 'Product tag', 'default': 'music' },
                { arg: 'limit', type: 'number', description: 'Limit of top products' },
                { arg: 'offset', type: 'number', description: 'Offset of top products' },
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: {
                arg: 'products', type: ['Product'], root: true, description: 'List of products ordered by purchases'
            },
            http: {path: '/top-selling', verb: 'get'}
        }
    )

    Product.remoteMethod(
        'getPromotedProducts',
        {
            description: 'Get promoted products',
            accepts: [
                { arg: 'tag', type: 'string', description: 'Product tag', 'default': 'music' },
                { arg: 'limit', type: 'number', description: 'Limit of products' },
                { arg: 'offset', type: 'number', description: 'Offset of products' },
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: {
                arg: 'products', type: ['Product'], root: true, description: 'List of promoted products'
            },
            http: {path: '/promoted', verb: 'get'}
        }
    )

    Product.remoteMethod(
        'countPromotedProducts',
        {
            description: 'Count promoted products',
            accepts: [
                { arg: 'tag', type: 'string', description: 'Product tag', 'default': 'music' },
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: {
                arg: 'products', type: 'object', root: true, description: 'Count of promoted products'
            },
            http: {path: '/promoted/count', verb: 'get'}
        }
    )

    Product.remoteMethod(
        'downloadSample',
        {
            description: 'Download product sample',
            accepts: [
                { arg: 'id', type: 'string', required: true, description: 'Product id', default: '57e090a1aa448818ebfe3f48' },
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            http: {path: '/:id/sample', verb: 'get'}
        }
    )

}
