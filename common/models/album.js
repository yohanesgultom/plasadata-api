module.exports = function(Album) {

    /***************** Disable REST API *****************/

    Album.disableRemoteMethod('create', true)
    Album.disableRemoteMethod('upsert', true)
    Album.disableRemoteMethod('updateAll', true)
    Album.disableRemoteMethod('deleteById', true)
    Album.disableRemoteMethod('replaceOrCreate', true)
    Album.disableRemoteMethod('createChangeStream', true)
    Album.disableRemoteMethod('updateAttributes', false)
    Album.disableRemoteMethod('replaceById', true)
    Album.disableRemoteMethod('upsertWithWhere', true)
    Album.disableRemoteMethod('__create__products', false)
    Album.disableRemoteMethod('__upsert__products', false)
    Album.disableRemoteMethod('__delete__products', false)
    Album.disableRemoteMethod('__updateById__products', false)
    Album.disableRemoteMethod('__destroyById__products', false)
    Album.disableRemoteMethod('__link__products', false)
    Album.disableRemoteMethod('__unlink__products', false)

}
