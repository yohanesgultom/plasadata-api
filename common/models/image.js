module.exports = function(Image) {

    Image.disableRemoteMethod("find", true)
    Image.disableRemoteMethod("findOne", true)
    Image.disableRemoteMethod("create", true)
    Image.disableRemoteMethod("upsert", true)
    Image.disableRemoteMethod("updateAll", true)
    Image.disableRemoteMethod("deleteById", true)
    Image.disableRemoteMethod("count", true)
    Image.disableRemoteMethod("exists", true)

    Image.download = function (container, file, ctx, cb) {
        Image.app.models.container.download(container, file, ctx.req, ctx.res, cb)
    }

    Image.remoteMethod(
        'download',
        {
            isStatic: true,
            description: 'Download image from "products" or "customers" container',
            accepts: [
                { arg: 'container', type: 'string', required: true, description: 'Container name ("products" or "customers")', default: 'products' },
                { arg: 'file', type: 'string', required: true, description: 'File name' },
                { arg: 'ctx', type: 'object', http: { source:'context' } },
            ],
            http: {path: '/:container/download/:file', verb: 'get'}
        }
    )

}
