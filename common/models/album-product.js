module.exports = function(AlbumProduct) {

    var app = require('../../server/server')

    AlbumProduct.observe('after save', function(ctx, next) {
        if (ctx.instance) {
            app.models.Album.findById(ctx.instance.albumId, function (err, album) {
                if (err) {
                    next(err)
                } else {
                    app.models.Product.findById(ctx.instance.productId, function (err, product) {
                        if (err) {
                            next(err)
                        } else {
                            // we only care about first tag
                            if (album.tags.indexOf(product.tags[0]) < 0) {
                                album.tags.push(product.tags[0])
                            }
                            album.updateAttribute('tags', album.tags, next)
                        }
                    })
                }
            })
        }
    })

    AlbumProduct.observe('after delete', function(ctx, next) {
        if (ctx.where) {
            app.models.Album.findById(ctx.where.albumId, {include: 'products'}, function (err, album) {
                if (err) {
                    next(err)
                } else {
                    var tags = []
                    album.products().forEach(function(product) {
                        if (tags.indexOf(product.tags[0]) < 0) {
                            tags.push(product.tags[0])
                        }
                    })
                    album.updateAttribute('tags', tags, next)
                }
            })
        }
    })
}
