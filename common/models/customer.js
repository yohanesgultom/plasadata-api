var CONTAINERS_URL = '/api/images/',
    DEFAULT_TTL = -1, // eternal
    MAX_TOKENS = 0, // <= 0 disabled
    REGISTRATION_BONUS = 20000

module.exports = function(Customer) {

    var app = require('../../server/server'),
        moment = require('moment'),
        request = require('request'),
        path = require('path'),
        config = require('../../server/config.json'),
        datasources = require('../../server/datasources.json'),
        VERITRANS = config.plasadata.VERITRANS,
        FACEBOOK = config.plasadata.FACEBOOK

    // Super important!
    // disable email validation
    // to allow third party login without email
    delete Customer.validations.email
    // validate email format
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    Customer.validate('email', function (err) { if (!re.test(this.email) && this.email !== undefined) err(); }, {message: 'Email format is invalid'})
    // keep email uniqueness validation
    Customer.validatesUniquenessOf('email', {message: 'Email already exists'});

    // Super important!
    // handle strange behaviour of decimal values
    Customer.afterInitialize = function() {
        var fields = ['balance'],
        that = this
        fields.forEach(function(f) {
            if (typeof that[f] === 'string' || that[f] instanceof String) {
                that[f] = parseFloat(that[f])
            }
        })
        // fullname
        var names = []
        if (this.firstName && this.firstName.length > 0) names.push(this.firstName)
        if (this.lastName && this.lastName.length > 0) names.push(this.lastName)
        this.fullName = names.join(' ')
    }

    Customer.getVeritransProperties = function () {
        return {
            MERCHANT_ID: VERITRANS.MERCHANT_ID,
            CLIENT_KEY: VERITRANS.CLIENT_KEY
        }
    }

    // get SNAP token from veritrans for specific order
    Customer.getVeritransSnapToken = function (orderId, amount, callback) {
        console.log('POST ' + VERITRANS.END_POINT_SNAP)
        var headerData = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + new Buffer(VERITRANS.SERVER_KEY + ':').toString('base64')
            },
            data = {
                'transaction_details': {
                    'order_id': orderId,
                    'gross_amount': amount
                }
            }

        // invoke veritrans api
        request.post({url: VERITRANS.END_POINT_SNAP, headers: headerData, body: JSON.stringify(data)}, function (err, httpResponse, body) {
            if (err) {
                console.error(err)
                if (callback) callback(err, null)
            } else {
                body = body ? JSON.parse(body) : body
                if (body && body.error_messages) {
                    console.error(body)
                    if (callback) callback(body.error_messages, null)
                } else {
                    console.error(body)
                    if (callback) callback(null, body.token)
                }
            }
        })
    }

    function randomAlphaNum(len) {
        var text = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
        for( var i=0; i < len; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length))
        return text
    }

    /***************** Validations *****************/

    Customer.validatesUniquenessOf('username', {message: 'username is not unique'})
    Customer.validatesUniquenessOf('email', {message: 'email is not unique'})

    /***************** Instance Methods *****************/

    Customer.prototype.purchaseProducts = function(recipient, products, bundles, cb) {
        var totalPrice = 0,
            promises = [],
            now = new Date(),
            customer = this

        // update all prices based on promotions
        for (var i=0; i < products.length; i++) {
            promises.push(products[i].setPrice())
        }

        Promise.all(promises).then(function(products){
            // get total price
            for (var i=0; i < products.length; i++) {
                totalPrice += products[i].price
            }

            // bundles price
            for (var i=0; i < bundles.length; i++) {
                totalPrice += bundles[i].price
            }

            // check if balance is sufficent
            if (totalPrice <= customer.balance) {
                Customer.beginTransaction({isolationLevel: Customer.Transaction.READ_COMMITED}, function(err, tx) {
                    if (err) rollback(tx, err, cb)
                    var purchases = [],
                        balanceHistories = [],
                        balanceCode,
                        balanceInfo,
                        giver,
                        gift = customer.id != recipient.id

                    // different parameter for purchase and gift
                    if (gift) {
                        balanceCode = app.models.BalanceHistory.STATUS.GIFT
                        balanceInfo = ' to ' + recipient.fullName
                    } else {
                        balanceCode = app.models.BalanceHistory.STATUS.PURCHASE
                        balanceInfo = ''
                    }

                    // create purchases & histories for each standalone products
                    products.forEach(function(p) {
                        // subscription
                        var startDate = null,
                            endDate = null,
                            duration = null
                        if (p.duration && !p.file) {
                            // expected value: "7 d", "1 w", "6 M" .etc
                            duration = p.duration.split(' ')
                            startDate = now
                            endDate = moment(now).add(parseInt(duration[0]), duration[1])
                        }

                        purchases.push({
                            customerId: recipient.id,
                            productId: p.id,
                            price: p.price,
                            productName: p.name,
                            promotions: p.promoExcerpts,
                            startDate: startDate,
                            endDate: endDate,
                            customerName: recipient.fullName,
                            basePrice: p.basePrice,
                            productTags: p.tags,
                            gift: gift
                        })

                        balanceHistories.push({
                            value: (p.price * -1),
                            code: balanceCode,
                            info: p.name + balanceInfo
                        })

                    })

                    // create purchase & histories for bundled products
                    bundles.forEach(function(b) {
                        b.products().forEach(function(p) {
                            var finalPrice = Math.round((p.basePrice - (p.basePrice * b.discount / 100)) * 100) / 100

                            purchases.push({
                                customerId: recipient.id,
                                productId: p.id,
                                price: finalPrice,
                                productName: p.name,
                                bundle: b.getExcerpt(),
                                customerName: recipient.fullName,
                                basePrice: p.basePrice,
                                productTags: p.tags,
                                gift: gift
                            })

                            balanceHistories.push({
                                value: (finalPrice * -1),
                                code: balanceCode,
                                info: p.name + balanceInfo
                            })

                        })

                    })

                    // persist purchases
                    app.models.Purchase.create(purchases, {transaction: tx}, function(err, purc) {
                        if (err) rollback(tx, err, cb)

                        // persist histories
                        customer.balanceHistories.create(balanceHistories, {transaction: tx}, function(err, hists) {
                            if (err) rollback(tx, err, cb)
                            recipient.purchaseHistories.create(purchases, {transaction: tx}, function(err, hists) {
                                if (err) rollback(tx, err, cb)
                                // reduce balance
                                customer.balance = customer.balance - totalPrice
                                customer.save(function(err, obj) {
                                    if (err) rollback(tx, err, cb)
                                    tx.commit(function(err) {
                                        if (err) rollback(tx, err, cb)
                                        cb(null, purc)
                                    })
                                })
                            })
                        })
                    })
                })
            } else {
                cb('insufficient balance: ' + totalPrice + ' needed ' + customer.balance + ' available', null)
            }
        }, function(err){
            cb(err, null)
        })

    }

    Customer.prototype.purchase = function(products, bundles, cb) {
        this.purchaseProducts(this, products, bundles, cb)
    }

    Customer.prototype.purchaseAsGift = function(recipient, products, bundles, cb) {
        this.purchaseProducts(recipient, products, bundles, cb)
    }

    Customer.prototype.refund = function(purchases, cb) {
        var customer = this
        Customer.beginTransaction({isolationLevel: Customer.Transaction.READ_COMMITED}, function(err, tx) {
            if (err) rollback(tx, err, cb)
            var promises = [],
            totalRefund = 0.0,
            purchaseIds = []
            for (var i=0; i < purchases.length; i++) {
                purchaseIds.push(purchases[i].id)
                totalRefund += purchases[i].price
            }
            app.models.Purchase.destroyAll({id: {inq: purchaseIds}}, {transaction: tx}, function(err, res) {
                if (err) rollback(tx, err, cb)
                // create history
                var balanceHistories = purchases.map(function(p) {
                    return {value: (p.price), code: app.models.BalanceHistory.STATUS.REFUND, info: p.name}
                })
                customer.balanceHistories.create(balanceHistories, {transaction: tx}, function(err, res) {
                    // add balance
                    customer.balance += totalRefund
                    customer.save(function(err, obj) {
                        if (err) rollback(tx, err, cb)
                        tx.commit(function(err) {
                            if (err) rollback(tx, err, cb)
                            cb(null, purchases)
                        })
                    })
                })
            })
        })
    }

    Customer.prototype.updateCollection = function (Model, modelId, productIds, cb) {
        var customer = this
        Model.findById(modelId, function(err, collection) {
            if (err) {
                cb(err, null)
            } else {
                // check if customer has purchased the products
                app.models.Purchase.find({where: {customerId: customer.id, productId: {inq: productIds}}, include: 'product'}, function (err, purchases) {
                    if (err) {
                        cb(err, null)
                    } else {
                        collection.products.destroyAll(function(err) {
                            if (err) {
                                cb(err, null)
                            } else {
                                var promises = []
                                purchases.forEach(function(p) {
                                    promises.push(new Promise(function (resolve, reject) {
                                        collection.products.add(p.product(), function(err, added) {
                                            if (err) {
                                                reject(err)
                                            } else {
                                                resolve(added)
                                            }
                                        })
                                    }))
                                })
                                Promise.all(promises).then(function (values) {
                                    cb(null, values)
                                }, function (error) {
                                    cb(error, null)
                                })
                            }
                        })
                    }
                })
            }
        })
    }

    Customer.prototype.topup = function (orderId, amount, cb) {
        // check if topup has been applied before
        var customer = this
        app.models.BalanceHistory.findOne({where: {info: orderId, value: amount}}, function (err, history) {
            if (err) {
                console.error(err)
                cb(err, null)
            } else if (history) {
                // added before, ignore
                cb(null, history)
            } else {
                // create balance history & add balance
                Customer.beginTransaction({isolationLevel: Customer.Transaction.READ_COMMITED}, function(err, tx) {
                    history = {
                        code: app.models.BalanceHistory.STATUS.TOPUP,
                        value: amount,
                        info: orderId
                    }
                    customer.balanceHistories.create(history, {transaction: tx}, function(err, newHistory) {
                        // add balance
                        customer.balance += amount
                        customer.save(function(err, obj) {
                            if (err) rollback(tx, err, cb)
                            tx.commit(function(err) {
                                if (err) rollback(tx, err, cb)
                                console.log(newHistory)
                                cb(null, newHistory)
                            })
                        })
                    })
                })
            }
        })
    }

    function rollback(tx, err, cb) {
        console.error(err)
        tx.rollback(function(rollbackerr) {
            if (rollbackerr) err = rollbackerr
            cb(err, null)
        })
        return false
    }

    /***************** Disable REST API *****************/

    Customer.disableRemoteMethod('find', true)
    Customer.disableRemoteMethod('findOne', true)
    Customer.disableRemoteMethod('upsert', true)
    Customer.disableRemoteMethod('updateAll', true)
    Customer.disableRemoteMethod('deleteById', true)
    Customer.disableRemoteMethod('count', true)
    Customer.disableRemoteMethod('exists', true)
    Customer.disableRemoteMethod('resetPassword', true)
    Customer.disableRemoteMethod('createChangeStream', true)
    Customer.disableRemoteMethod('replaceOrCreate', true)
    Customer.disableRemoteMethod('upsertWithWhere', true)
    Customer.disableRemoteMethod('__create__accessTokens', false)
    Customer.disableRemoteMethod('__delete__accessTokens', false)
    Customer.disableRemoteMethod('__upsert__accessTokens', false)
    Customer.disableRemoteMethod('__destroyById__accessTokens', false)
    Customer.disableRemoteMethod('__updateById__accessTokens', false)
    Customer.disableRemoteMethod('__findById__balanceHistories', false)
    Customer.disableRemoteMethod('__create__roles', false)
    Customer.disableRemoteMethod('__upsert__roles', false)
    Customer.disableRemoteMethod('__delete__roles', false)
    Customer.disableRemoteMethod('__updateById__roles', false)
    Customer.disableRemoteMethod('__destroyById__roles', false)
    Customer.disableRemoteMethod('__link__roles', false)
    Customer.disableRemoteMethod('__unlink__roles', false)
    Customer.disableRemoteMethod('__create__purchases', false)
    Customer.disableRemoteMethod('__upsert__purchases', false)
    Customer.disableRemoteMethod('__delete__purchases', false)
    Customer.disableRemoteMethod('__updateById__purchases', false)
    Customer.disableRemoteMethod('__destroyById__purchases', false)
    Customer.disableRemoteMethod('__create__balanceHistories', false)
    Customer.disableRemoteMethod('__upsert__balanceHistories', false)
    Customer.disableRemoteMethod('__delete__balanceHistories', false)
    Customer.disableRemoteMethod('__updateById__balanceHistories', false)
    Customer.disableRemoteMethod('__destroyById__balanceHistories', false)
    Customer.disableRemoteMethod('__create__purchaseHistories', false)
    Customer.disableRemoteMethod('__upsert__purchaseHistories', false)
    Customer.disableRemoteMethod('__delete__purchaseHistories', false)
    Customer.disableRemoteMethod('__updateById__purchaseHistories', false)
    Customer.disableRemoteMethod('__destroyById__purchaseHistories', false)
    Customer.disableRemoteMethod('__create__products', false)
    Customer.disableRemoteMethod('__upsert__products', false)
    Customer.disableRemoteMethod('__delete__products', false)
    Customer.disableRemoteMethod('__updateById__products', false)
    Customer.disableRemoteMethod('__destroyById__products', false)
    Customer.disableRemoteMethod('__link__products', false)
    Customer.disableRemoteMethod('__unlink__products', false)
    Customer.disableRemoteMethod('__create__cart', false)
    Customer.disableRemoteMethod('__destroy__cart', false)
    Customer.disableRemoteMethod('__create__wishlists', false)
    Customer.disableRemoteMethod('__delete__wishlists', false)
    Customer.disableRemoteMethod('__upsert__wishlists', false)
    Customer.disableRemoteMethod('__updateById__wishlists', false)
    Customer.disableRemoteMethod('__destroyById__wishlists', false)
    Customer.disableRemoteMethod('__findById__playlists', false)
    Customer.disableRemoteMethod('__create__messages', false)
    Customer.disableRemoteMethod('__upsert__messages', false)
    Customer.disableRemoteMethod('__updateById__messages', false)

    /***************** Remote Hooks *****************/

    // set time to live (ttl) and validate number of access tokens
    Customer.beforeRemote('login', function(ctx, modelInstance, next) {
        // force TTL
        ctx.args.credentials.ttl = DEFAULT_TTL
        Customer.findOne({where: {username: ctx.args.credentials.username}}, function (err, customer) {
            if (err) {
                next(err)
            } else {
                app.models.CustomerToken.find({where: {userId: customer.id}}, function (err, tokens) {
                    if (err) {
                        next(err)
                    } else if (tokens && MAX_TOKENS > 0 && tokens.length >= MAX_TOKENS) {
                        next('Max tokens reached for user ' + ctx.args.credentials.username + ': ' + tokens.length)
                    } else {
                        next()
                    }
                })
            }
        })
    })

    // set default role
    Customer.afterRemote('create', function(ctx, modelInstance, next) {
        // find buyer role and set as default
        app.models.Role.findOne({where: {name: 'buyer'}}, function (err, buyerRole) {
            if (err) next(err)
            buyerRole.principals.create({
                principalType: app.models.RoleMapping.USER,
                principalId: modelInstance.id
            }, function (err, res) {
                if (err) next(err)
                // add registration bonus balance
                modelInstance.balance += REGISTRATION_BONUS
                modelInstance.emailVerified = false
                modelInstance.save(function(err) {
                    if (err) next(err)

                    if (datasources.mailDS && datasources.mailDS.transports[0].auth.user) {
                        // send verification email
                        var options = {
                          type: 'email',
                          to: modelInstance.email,
                          from: datasources.mailDS.transports[0].auth.user,
                          subject: 'Welcome to Plasa Data',
                          template: path.resolve(__dirname, '../../server/views/verify.ejs'),
                          redirect: '/verified',
                          user: modelInstance,
                          host: config.plasadata.server.host,
                          port: config.plasadata.server.port,
                        }

                        modelInstance.verify(options, function(err, response) {
                            if (err) {
                                Customer.deleteById(modelInstance.id);
                                return next(err);
                            } else {
                                next()
                            }
                        })
                    } else {
                        console.error('MailDS is not configured. No verification email sent')
                        next()
                    }
                })
            })
        })
    })

    // create cart automatically when not yet exist
    Customer.beforeRemote('prototype.__get__cart', function(ctx, modelInstance, next) {
        app.models.Cart.findOrCreate({ where: {customerId: ctx.req.params.id}}, {customerId: ctx.req.params.id}, function(err, cart) {
            next(err)
        })
    })

    // sync cart
    Customer.beforeRemote('prototype.__update__cart', function(ctx, modelInstance, next) {
        // delete immutable field input
        delete ctx.req.body.id
        delete ctx.req.body.customerId

        // create cart if not yet exists
        app.models.Cart.findOrCreate({ where: {customerId: ctx.req.params.id}}, {customerId: ctx.req.params.id}, function(err, cart) {
            if (err) {
                next(err)
            } else {
                var productIds = ctx.req.body.products,
                bundleIds = ctx.req.body.bundles
                app.models.Cart.getObjectsByIds(productIds, bundleIds).then(function(objects) {
                    // validate duplicate products in bundles
                    [conflictBundles, products, bundles] = app.models.Cart.getConflictBundles(objects)
                    if (Object.keys(conflictBundles).length > 0) {
                        next('Bundle(s) conflict')
                    } else {
                        next()
                    }
                }, function(err) {
                    next(err)
                })
            }
        })

    })

    Customer.beforeRemote('prototype.__create__playlists', function(ctx, modelInstance, next) {
        // delete immutable field input
        delete ctx.req.body.id
        delete ctx.req.body.customerId
        ctx.args.data = ctx.req.body
        next()
    })


    Customer.beforeRemote('prototype.__updateById__playlists', function(ctx, modelInstance, next) {
        // delete immutable field input
        delete ctx.req.body.id
        delete ctx.req.body.customerId
        ctx.args.data = ctx.req.body
        next()
    })

    /***************** REST API *****************/

    // photo upload
    Customer.photoUpload = function (id, ctx, cb) {
        ctx.req.params.container = 'customers'
        Customer.findById(id, function(err, customer) {
            app.models.container.upload(ctx.req, ctx.res, function (err, fileObj) {
                if(err) {
                    cb(err)
                } else {
                    var fileInfo = null
                    for (var key in fileObj.files) {
                        fileInfo = fileObj.files[key][0]
                    }
                    customer.profileImage = CONTAINERS_URL + fileInfo.container + '/download/' + fileInfo.name
                    customer.save(function (err, customer) {
                        cb(err, customer.profileImage)
                    })
                }
            })
        })
    }

    // purchase
    Customer.purchase = function (id, pid, ctx, cb) {
        Customer.findById(id, function(err, customer) {
            if (err) {
                cb(err, null)
            } else {
                app.models.Product.findById(pid, function(err, prod) {
                    if (err) {
                        cb(err, null)
                    } else if (!prod) {
                        err = new Error()
                        err.status = 401
                        err.message = 'UNAUTHORIZED'
                        cb(err, null)
                    } else {
                        customer.purchase([prod], [], cb)
                    }
                })
            }
        })
    }

    // purchase bundle
    Customer.purchaseBundle = function (id, pid, ctx, cb) {
        Customer.findById(id, function(err, customer) {
            if (err) {
                cb(err, null)
            } else {
                app.models.Bundle.findById(pid, function(err, bundle) {
                    if (err) {
                        cb(err, null)
                    } else if (!bundle) {
                        err = new Error()
                        err.status = 401
                        err.message = 'UNAUTHORIZED'
                        cb(err, null)
                    } else {
                        customer.purchase([], [bundle], cb)
                    }
                })
            }
        })
    }

    // refund
    Customer.refund = function (id, data, ctx, cb) {
        var purchaseIds = data.purchases
        Customer.findById(id, function(err, customer) {
            if (err) {
                cb(err, null)
            } else {
                app.models.Purchase.find({where: {id: {inq: purchaseIds}, customerId: id}}, function(err, purchases) {
                    if (err) {
                        cb(err, null)
                    } else if (!purchases || purchases.length <= 0) {
                        err = new Error()
                        err.status = 401
                        err.message = 'UNAUTHORIZED'
                        cb(err, null)
                    } else {
                        customer.refund(purchases, cb)
                    }
                })
            }
        })
    }

    // validate cart
    Customer.validateCart = function (id, data, ctx, cb) {
        Customer.findById(id, function(err, customer) {
            if (err) {
                cb(err, null)
            } else {
                customer.cart(function(err, cart) {
                    if (err) {
                        cb(err, null)
                    } else {
                        app.models.Cart.validate(data.products, data.bundles, function(err, conflicts) {
                            if (err) {
                                cb(err, null)
                            } else {
                                var res = []
                                Object.keys(conflicts).forEach(function(bundleId){
                                    res.push({
                                        bundleId: bundleId,
                                        productIds: conflicts[bundleId]
                                    })
                                })
                                cb(null, res)
                            }
                        })
                    }
                })
            }
        })
    }

    // checkout
    Customer.checkout = function (id, ctx, cb) {
        Customer.findById(id, function(err, customer) {
            if (err) {
                cb(err, null)
            } else {
                customer.cart(function(err, cart) {
                    if (err) {
                        cb(err, null)
                    } else {
                        cart.checkout(customer, cb)
                    }
                })
            }
        })
    }

    // find playlist by id
    Customer.__findById__playlists = function(customerId, playlistId, ctx, cb) {
        app.models.Playlist.findOne({where: {id: playlistId, customerId: customerId}, include: 'products'}, cb)
    }

    // sync playlist's item
    Customer.updatePlaylist = function (id, playlistId, data, ctx, cb) {
        var productIds = data.products
        Customer.findById(id, function(err, customer) {
            if (err) {
                cb(err, null)
            } else {
                customer.updateCollection(app.models.Playlist, playlistId, productIds, cb)
            }
        })
    }

    // secure content download
    Customer.download = function (id, pid, ctx, cb) {
        app.models.Purchase.findOne({where: {customerId: id, productId: pid}, include: 'product'}, function (err, purchase) {
            if (err) {
                cb(err, null)
            } else if (!purchase) {
                err = new Error()
                err.status = 404
                err.message = 'Purchase not found'
                cb(err, null)
            } else {
                var product = purchase.product()
                app.models.contentcontainer.download('files', product.file, ctx.req, ctx.res, cb)
            }
        })
    }

    Customer.downloadSubscriptionItem = function (customerId, itemId, ctx, cb) {
        // get the subscription item
        var now = new Date()
        app.models.SubscriptionItem.findById(itemId, function(err, item) {
            if (err) {
                cb(err, null)
            } else if (!item || !item.file) {
                err = new Error()
                err.status = 401
                err.message = 'UNAUTHORIZED'
                cb(err, null)
            } else {
                // check if the customer owns the subscription
                // and the current date is still between
                // subscription startDate and endDate (in Purchase)
                app.models.Purchase.findOne({where: {customerId: customerId, productId: item.productId, startDate: {lte: now}, endDate: {gte: now}}}, function(err, purchase) {
                    if (err) {
                        cb(err, null)
                    } else if (!purchase) {
                        err = new Error()
                        err.status = 401
                        err.message = 'UNAUTHORIZED'
                        cb(err, null)
                    } else {
                        app.models.contentcontainer.download('files', item.file, ctx.req, ctx.res, cb)
                    }
                })
            }
        })
    }


    // mark messages
    Customer.markMessages = function (userId, data, ctx, cb) {
        var messageIds = data.messageIds,
            status = data.status

        app.models.Message.updateAll({customerId: userId, id: {inq: messageIds}}, {status: status}, function (err, info) {
            if (err) {
                cb(err, null)
            } else {
                ctx.res.statusCode = 204
                cb(null, null)
            }
        })
    }

    // get veritrans keys and order id for balance top up
    Customer.topup = function (userId, data, ctx, cb) {
        console.log(data)
        var messageIds = data.messageIds,
            amount = data.amount,
            orderId = orderId = 'TOP_' + userId + '_' + new Date().getTime(),
            res = {}

        // invoke veritrans snap api to get token
        Customer.getVeritransSnapToken(orderId, amount, function (err, token) {
            if (err) {
                cb(err, null)
            } else {
                var newPayment = {
                    orderId: orderId,
                    customerId: userId,
                    status: app.models.Payment.STATUS.STARTED
                }
                app.models.Payment.create(newPayment, function (err, payment) {
                    if (err) {
                        console.error(err)
                        cb(err, null)
                    } else { // return tokens and detail
                        res.veritransSnapToken = token
                        res.veritransClientKey = VERITRANS.CLIENT_KEY
                        res.orderId = orderId
                        res.amount = amount
                        cb(null, res)
                    }
                })
            }
        })
    }

    // get topup status
    Customer.topupStatus = function (customerId, orderId, ctx, cb) {
        console.log('Order ID: ' + orderId)
        var headerData = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + new Buffer(VERITRANS.SERVER_KEY + ':').toString('base64')
            },
            url = VERITRANS.END_POINT + '/' + orderId + '/status'

        app.models.Payment.findOne({where: {orderId: orderId, status: app.models.Payment.STATUS.CONFIRMED}}, function (err, payment) {
            if (err) {
                cb(err, null)
            } else if (payment) { // return existing
                cb(null, payment)
            } else { // invoke veritrans api
                console.log('GET ' + url)
                request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
                        if (err) {
                            console.error(err)
                            cb(err, null)
                        } else {
                            body = body ? JSON.parse(body) : body
                            if (body && body.error_messages) {
                                console.error(body)
                                cb(body.error_messages, null)
                            } else {
                                // create a confirmed payment
                                app.models.Payment.findOne({where: {orderId: orderId, status: app.models.Payment.STATUS.STARTED}}, function (err, payment) {
                                    if (err) {
                                        cb(err, null)
                                    } else if (payment) {
                                        app.models.Payment.create({
                                            orderId: payment.orderId,
                                            customerId: payment.customerId,
                                            payload: body,
                                            status: app.models.Payment.STATUS.CONFIRMED
                                        }, function (err, newPayment) {
                                            if (err) {
                                                cb(err, null)
                                            } else {
                                                // find customer and do balance topup
                                                Customer.findById(customerId, function(err, customer) {
                                                    if (err) {
                                                        cb(err, null)
                                                    } else {
                                                        var amount = parseFloat(newPayment.payload.gross_amount)
                                                        customer.topup(newPayment.orderId, amount, cb)
                                                    }
                                                })
                                            }
                                        })
                                    } else {
                                        // handle strange case for tracing purpose: let customerId be empty
                                        // no need to add balance
                                        app.models.Payment.create({
                                            orderId: orderId,
                                            payload: body,
                                            status: app.models.Payment.STATUS.CONFIRMED
                                        }, function (err, newPayment) {
                                            cb('No corresponding orderId: ' + orderId, null)
                                        })
                                    }
                                })

                            }
                        }
                    }
                )
            }
        })
    }

    // reset password
    // TODO improve security (eg. someone may mess with other user when they know the username/email)
    Customer.resetPasswordRandom = function (data, ctx, cb) {
        var criteria = {}
        if (!data.email && !data.username) {
            cb('No email nor username provided', null)
        } else {
            if (data.email) {
                criteria = {email: data.email}
            } else { // if (data.username)
                criteria = {username: data.username}
            }
            Customer.findOne({where: criteria}, function (err, customer) {
                if (err) {
                    cb(err, null)
                } else if (!customer) {
                    cb('Customer not found', null)
                } else if (!customer.email) {
                    cb('Customer has no email', null)
                } else {
                    // generate random password and save
                    var randomPassword = randomAlphaNum(10)
                    customer.password = randomPassword
                    customer.save(function(err, updatedCustomer) {
                        if (err) {
                            cb(err, null)
                        } else {
                            // send password by email
                            app.models.Email.send({
                              to: customer.email,
                              from: datasources.mailDS.transports[0].auth.user,
                              subject: 'Password reset for Plasa Data',
                              html: 'Your password has been reset to: ' + randomPassword
                            }, function(err) {
                              if (err) return console.error('> error sending password reset email');
                              console.error('> sending password reset email to:', customer.email);
                              ctx.res.status(204).send()
                            })
                        }
                    })
                }
            })
        }
    }

    // purchase as gift
    Customer.purchaseAsGift = function (id, recipientUsername, pid, ctx, cb) {
        Customer.findById(id, function(err, customer) {
            if (err) {
                cb(err, null)
            } else {
                Customer.findOne({where: {username: recipientUsername}}, function(err, recipient) {
                    if (err) {
                        cb(err, null)
                    } else if (!recipient) {
                        err = new Error()
                        err.status = 404
                        err.message = 'Recipient not found'
                        cb(err, null)
                    } else {
                        app.models.Product.findById(pid, function(err, prod) {
                            if (err) {
                                cb(err, null)
                            } else if (!prod) {
                                err = new Error()
                                err.status = 404
                                err.message = 'Product not found'
                                cb(err, null)
                            } else {
                                customer.purchaseAsGift(recipient, [prod], [], cb)
                            }
                        })
                    }
                })
            }
        })
    }

    // purchase bundle as gift
    Customer.purchaseBundleAsGift = function (id, recipientUsername, pid, ctx, cb) {
        Customer.findById(id, function(err, customer) {
            if (err) {
                cb(err, null)
            } else {
                Customer.findOne({where: {username: recipientUsername}}, function(err, recipient) {
                    if (err) {
                        cb(err, null)
                    } else if (!recipient) {
                        err = new Error()
                        err.status = 404
                        err.message = 'Recipient not found'
                        cb(err, null)
                    } else {
                        app.models.Bundle.findById(pid, function(err, bundle) {
                            if (err) {
                                cb(err, null)
                            } else if (!bundle) {
                                err = new Error()
                                err.status = 401
                                err.message = 'UNAUTHORIZED'
                                cb(err, null)
                            } else {
                                customer.purchaseAsGift(recipient, [], [bundle], cb)
                            }
                        })
                    }
                })
            }
        })
    }

    // checkout
    Customer.checkoutAsGift = function (id, recipientUsername, ctx, cb) {
        Customer.findById(id, function(err, customer) {
            if (err) {
                cb(err, null)
            } else {
                Customer.findOne({where: {username: recipientUsername}}, function(err, recipient) {
                    if (err) {
                        cb(err, null)
                    } else if (!recipient) {
                        err = new Error()
                        err.status = 404
                        err.message = 'Recipient not found'
                        cb(err, null)
                    } else {
                        customer.cart(function(err, cart) {
                            if (err) {
                                cb(err, null)
                            } else {
                                cart.checkoutAsGift(customer, recipient, cb)
                            }
                        })
                    }
                })
            }
        })
    }

    // third party login handler
    Customer.thirdPartyLogin = function (data, ctx, cb) {
        var error = new Error()
        if (!data.provider || !data.user_access_token || !data.user_id) {
            error.status = 400
            cb(error, null)
        } else {
            var userToken = data.user_access_token,
                userId = data.user_id,
                userEmail = data.email ? data.email : null,
                appToken = FACEBOOK.APP_TOKEN,
                userPassword,
                url
            if (data.provider == 'facebook') {
                url = FACEBOOK.END_POINT + '/debug_token?input_token=' + userToken + '&access_token=' + appToken
                console.log('GET ' + url)
                request.get({url: url}, function (err, httpResponse, body) {
                    if (err) {
                        console.log(err);
                        cb(err, null)
                    } else {
                        body = JSON.parse(body)
                        if (!body.data || !body.data.is_valid) {
                            error.status = 401
                            cb(error, null)
                        } else {
                            userPassword = userToken + '__' + appToken
                            Customer.findOne({where: {username: userId}}, function (err, cust) {
                                if (err) {
                                    console.log(err);
                                    cb(err, null)
                                } else if (!cust) {
                                    cust = new Customer()
                                    cust.username = userId
                                    if (userEmail) cust.email = userEmail
                                    cust.emailVerified = true
                                }
                                // keep renewing the password so user can login
                                cust.password = userPassword
                                cust.fbDeviceId = userToken
                                cust.save(function (err, obj) {
                                    if (err) {
                                        cb(err, null)
                                    } else {
                                        Customer.login({username: obj.username, password: userPassword, ttl: DEFAULT_TTL}, function (err, accessToken) {
                                            if (err) {
                                                cb(err, null)
                                            } else {
                                                cb(null, accessToken)
                                            }
                                        })
                                    }
                                })
                            })
                        }
                    }
                })
            } else {
                error.status = 400
                cb(error, null)
            }
        }
    }

    /***************** Remote methods *****************/

    Customer.remoteMethod(
        'photoUpload',
        {
            description: 'Uploads customer profile photo',
            accepts: [
                { arg: 'id', type: 'number', required: true, description: 'Customer id', default: '2' },
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: {
                arg: 'url', type: 'string'
            },
            http: {path: '/:id/photo-upload', verb: 'post'}
        }
    )

    Customer.remoteMethod(
        'purchase',
        {
            description: 'Purchase a product',
            accepts: [
                { arg: 'id', type: 'number', required: true, description: 'Customer id', default: '2' },
                { arg: 'fk', type: 'string', required: true, description: 'Product id', default: '57e090a1aa448818ebfe3f48' },
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: [
                { arg: 'purchases', type: 'object', root: true}
            ],
            http: {path: '/:id/purchase/:fk', verb: 'post'}
        }
    )

    Customer.remoteMethod(
        'purchaseBundle',
        {
            description: 'Purchase a bundle of products',
            accepts: [
                { arg: 'id', type: 'number', required: true, description: 'Customer id', default: '2' },
                { arg: 'fk', type: 'string', required: true, description: 'Bundle id', default: '57e090a1aa448818ebfe3f48' },
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: [
                { arg: 'purchases', type: 'object', root: true}
            ],
            http: {path: '/:id/purchase-bundle/:fk', verb: 'post'}
        }
    )

    Customer.remoteMethod(
        'refund',
        {
            description: 'Refund purchase(s)',
            accepts: [
                { arg: 'id', type: 'number', required: true, description: 'Customer id', default: '2' },
                { arg: 'data', type: 'any', required: true, description: '{purchases: [1, 2]}', default: {purchases: [1, 2]}, http: { source: 'body' }},
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: [
                { arg: 'purchases', type: 'array', root: true}
            ],
            http: {path: '/:id/refund', verb: 'post'}
        }
    )

    Customer.remoteMethod(
        'checkout',
        {
            description: 'Checkout current shopping cart',
            accepts: [
                { arg: 'id', type: 'number', required: true, description: 'Customer id', default: '2' },
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: [
                { arg: 'purchases', type: 'object', root: true }
            ],
            http: {path: '/:id/checkout', verb: 'post'}
        }
    )

    Customer.remoteMethod(
        '__findById__playlists',
        {
            description: 'Get playlist by id',
            accepts: [
                { arg: 'id', type: 'number', required: true, description: 'Customer id', default: '2' },
                { arg: 'fk', type: 'number', required: true, description: 'Album id', default: '1' },
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: [
                { arg: 'products', type: 'Playlist', root: true }
            ],
            http: {path: '/:id/playlists/:fk', verb: 'get'}
        }
    )

    Customer.remoteMethod(
        'updatePlaylist',
        {
            description: 'Update (sync) playlist\'s items',
            accepts: [
                { arg: 'id', type: 'number', required: true, description: 'Customer id', default: '2' },
                { arg: 'fk', type: 'number', required: true, description: 'Playlist id', default: '1' },
                { arg: 'data', type: 'any', required: true, description: '{products: ["58a4451cd608ae7634e2e565", "58a4451cd608ae7634e2e566"]}', default: {products: ["58a4451cd608ae7634e2e565", "58a4451cd608ae7634e2e566"]}, http: { source: 'body' }},
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: [
                { arg: 'products', type: 'object', root: true }
            ],
            http: {path: '/:id/playlists/:fk/sync', verb: 'put'}
        }
    )

    Customer.remoteMethod(
        'download',
        {
            description: 'Download purchased product',
            accepts: [
                { arg: 'id', type: 'number', required: true, description: 'Customer id', default: '2' },
                { arg: 'fk', type: 'string', required: true, description: 'Product id', default: '57e090a1aa448818ebfe3f48' },
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            http: {path: '/:id/products/:fk/download', verb: 'get', errorStatus: 403}
        }
    )

    Customer.remoteMethod(
        'downloadSubscriptionItem',
        {
            description: 'Download subscribed item',
            accepts: [
                { arg: 'id', type: 'number', required: true, description: 'Customer id', default: '2' },
                { arg: 'fk', type: 'string', required: true, description: 'Subscribed id', default: '57e090a1aa448818ebfe3f48' },
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            http: {path: '/:id/subscriptionItems/:fk/download', verb: 'get', errorStatus: 403}
        }
    )

    Customer.remoteMethod(
        'markMessages',
        {
            description: 'Mark messages (0: unread, 1: read)',
            accepts: [
                { arg: 'id', type: 'number', required: true, description: 'Customer id', default: '2' },
                { arg: 'data', type: 'any', required: true, description: '{messagesIds: [], status: 1}', default: {messagesIds: [], status: 1}, http: { source: 'body' }},
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            http: {path: '/:id/messages', verb: 'put'}
        }
    )

    Customer.remoteMethod(
        'validateCart',
        {
            description: 'Validate cart',
            accepts: [
                { arg: 'id', type: 'number', required: true, description: 'Customer id', default: '2' },
                { arg: 'data', type: 'any', required: true, description: '{products: [], bundles: []}', default: {products: [], bundles: []}, http: { source: 'body' }},
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: { arg: 'body', type: 'object', root: true },
            http: {path: '/:id/cart/validate', verb: 'post'}
        }
    )

    Customer.remoteMethod(
        'topup',
        {
            description: 'Get Veritrans Snap keys and order id to initiate top up flow',
            accepts: [
                { arg: 'id', type: 'number', required: true, description: 'Customer id', default: '2' },
                { arg: 'data', type: 'any', required: true, description: [
                    'Request: {amount: number}',
                    'Response: {veritransSnapToken: string, veritransClientKey: string, orderId: string, amount: number}'
                ], default: {amount: 100000}, http: { source: 'body' }},
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: { arg: 'body', type: 'object', root: true, default: {veritransSnapToken: '', veritransClientKey: '', orderId: '', amount: 0} },
            http: {path: '/:id/topup', verb: 'post'}
        }
    )

    Customer.remoteMethod(
        'topupStatus',
        {
            description: 'Get Veritrans payment status by order id and trigger balance update (in case not yet updated)',
            accepts: [
                { arg: 'id', type: 'number', required: true, description: 'Customer id', default: '2' },
                { arg: 'orderid', type: 'string', required: true, description: 'Snap topup order id', default: 'TOP-1483009363498' },
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: { arg: 'body', type: 'object', root: true, default: {value: 2000000, code: "TOPUP", info: "orderId", date: "2017-01-03T13:05:39.201Z", id: "586ba1a365cfff11e35b84e0", customerId: 2} },
            http: {path: '/:id/topup-status/:orderid', verb: 'get'}
        }
    )

    Customer.remoteMethod(
        'resetPasswordRandom',
        {
            description: 'Reset password by providing email or username',
            accepts: [
                { arg: 'data', type: 'any', required: true, description: '{email: string, username: string}', default: {email: '', username: ''}, http: { source: 'body' }},
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: { arg: 'body', type: 'object', root: true },
            http: {path: '/reset-password', verb: 'post'}
        }
    )

    Customer.remoteMethod(
        'purchaseAsGift',
        {
            description: 'Purchase a product as gift',
            accepts: [
                { arg: 'id', type: 'number', required: true, description: 'Customer id', default: '2' },
                { arg: 'recipientUsername', type: 'string', required: true, description: 'Recipient username', default: 'buyer4' },
                { arg: 'fk', type: 'string', required: true, description: 'Product id', default: '57e090a1aa448818ebfe3f48' },
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: [
                { arg: 'purchases', type: 'object', root: true}
            ],
            http: {path: '/:id/purchase/:fk/gift/:recipientUsername', verb: 'post'}
        }
    )

    Customer.remoteMethod(
        'purchaseBundleAsGift',
        {
            description: 'Purchase a bundle of products as gift',
            accepts: [
                { arg: 'id', type: 'number', required: true, description: 'Customer id', default: '2' },
                { arg: 'recipientUsername', type: 'string', required: true, description: 'Recipient username', default: 'buyer4' },
                { arg: 'fk', type: 'string', required: true, description: 'Bundle id', default: '57e090a1aa448818ebfe3f48' },
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: [
                { arg: 'purchases', type: 'object', root: true}
            ],
            http: {path: '/:id/purchase-bundle/:fk/gift/:recipientUsername', verb: 'post'}
        }
    )

    Customer.remoteMethod(
        'checkoutAsGift',
        {
            description: 'Checkout current shopping cart as gift',
            accepts: [
                { arg: 'id', type: 'number', required: true, description: 'Customer id', default: '2' },
                { arg: 'recipientUsername', type: 'string', required: true, description: 'Recipient username', default: 'buyer4' },
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: [
                { arg: 'purchases', type: 'object', root: true }
            ],
            http: {path: '/:id/checkout-gift/:recipientUsername', verb: 'post'}
        }
    )

    Customer.remoteMethod(
        'thirdPartyLogin',
        {
            description: 'Login or register using third party authentication (currently supported: facebook)',
            accepts: [
                { arg: 'data', type: 'any', required: true, description: '{provider: string, user_access_token: string, user_id: string, email: string}', default: '{"provider": "facebook", "user_access_token": "", "user_id": "", "email": ""}', http: { source: 'body' }},
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: [
                { arg: 'token', type: 'CustomerToken', root: true}
            ],
            http: {path: '/third-party-login', verb: 'post'}
        }
    )

}
