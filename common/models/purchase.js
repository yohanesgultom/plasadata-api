module.exports = function(Purchase) {

    // Super important!
    // handle strange behaviour of decimal values
    Purchase.afterInitialize = function() {
        var fields = ['price'],
            that = this
        fields.forEach(function(f) {
            if (typeof that[f] === 'string' || that[f] instanceof String) {
                that[f] = parseFloat(that[f])
            }
        })
    }

}
