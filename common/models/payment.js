module.exports = function(Payment) {

    var SHA512 = require('crypto-js/sha512'),
        app = require('../../server/server'),
        config = require('../../server/config.json'),
        VERITRANS = config.plasadata.VERITRANS

    Payment.STATUS = {
        STARTED: 'Started',
        FINISHED: 'Finished',
        NOTIFIED: 'Notified',
        CONFIRMED: 'Confirmed',
        UNFINISHED: 'Unfinished',
        ERROR: 'Error'
    }

    // capture payment finish redirection
    Payment.finish = function (ctx, cb) {
        var params = ctx.req.query
        Payment.findOne({where: {orderId: params.order_id, status: Payment.STATUS.FINISHED}}, function (err, payment) {
            if (err) {
                console.error(err)
                cb(err, null)
            } else if (payment) {
                cb(null, payment)
            } else {
                // create one
                Payment.findOne({where: {orderId: params.order_id}}, function (err, payment) {
                    if (err) {
                        console.error(err)
                        cb(err, null)
                    } else if (!payment) {
                        cb('Invalid order id', null)
                    } else {
                        Payment.create({
                            orderId: payment.orderId,
                            customerId: payment.customerId,
                            payload: params,
                            status: Payment.STATUS.FINISHED
                        }, cb)
                    }
                })
            }
        })
    }

    // capture payment finish redirection
    Payment.unfinish = function (ctx, cb) {
        var params = ctx.req.query
        Payment.findOne({where: {orderId: params.order_id, status: Payment.STATUS.UNFINISHED}}, function (err, payment) {
            if (err) {
                console.error(err)
                cb(err, null)
            } else if (payment) {
                cb(null, payment)
            } else {
                // create one
                Payment.findOne({where: {orderId: params.order_id}}, function (err, payment) {
                    if (err) {
                        console.error(err)
                        cb(err, null)
                    } else if (!payment) {
                        cb('Invalid order id', null)
                    } else {
                        Payment.create({
                            orderId: payment.orderId,
                            customerId: payment.customerId,
                            payload: params,
                            status: Payment.STATUS.UNFINISHED
                        }, cb)
                    }
                })
            }
        })
    }

    // capture payment finish redirection
    Payment.error = function (ctx, cb) {
        var params = ctx.req.query
        Payment.findOne({where: {orderId: params.order_id, status: Payment.STATUS.ERROR}}, function (err, payment) {
            if (err) {
                console.error(err)
                cb(err, null)
            } else if (payment) {
                cb(null, payment)
            } else {
                // create one
                Payment.findOne({where: {orderId: params.order_id}}, function (err, payment) {
                    if (err) {
                        console.error(err)
                        cb(err, null)
                    } else if (!payment) {
                        cb('Invalid order id', null)
                    } else {
                        Payment.create({
                            orderId: payment.orderId,
                            customerId: payment.customerId,
                            payload: params,
                            status: Payment.STATUS.ERROR
                        }, cb)
                    }
                })
            }
        })
    }

    // capture payment notification
    Payment.notification = function (data, ctx, cb) {
        // validate signature key
        var plain = data.order_id + data.status_code + data.gross_amount + VERITRANS.SERVER_KEY,
            hash = SHA512(plain),
            orderId = data.order_id
        console.log(data)
        if (hash == data.signature_key) {
            // check existing
            Payment.findOne({where: {orderId: orderId, status: Payment.STATUS.NOTIFIED}}, function (err, payment) {
                if (err) {
                    console.error(err)
                    cb(err, null)
                } else if (payment) { // return existing
                    cb(null, payment)
                } else { // save notification
                    Payment.findOne({where: {orderId: orderId, status: Payment.STATUS.STARTED}}, function (err, payment) {
                        if (err) {
                            cb(err, null)
                        } else if (payment) {
                            Payment.create({
                                orderId: payment.orderId,
                                customerId: payment.customerId,
                                payload: data,
                                status: Payment.STATUS.NOTIFIED
                            }, function (err, newPayment) {
                                // find customer and do balance topup
                                app.models.Customer.findById(payment.customerId, function(err, customer) {
                                    if (err) {
                                        cb(err, null)
                                    } else {
                                        var amount = parseFloat(newPayment.payload.gross_amount)
                                        customer.topup(newPayment.orderId, amount, cb)
                                    }
                                })
                            })
                        } else {
                            // handle strange case for tracing purpose: let customerId be empty
                            console.error('Invalid order id: ' + orderId)
                            Payment.create({
                                orderId: orderId,
                                payload: data,
                                status: Payment.STATUS.NOTIFIED
                            }, function (err, newPayment) {
                                cb('No corresponding orderId: ' + orderId, null)
                            })
                        }
                    })
                }
            })
        } else {
            var error = 'Unmatched signature'
            console.error(error)
            cb(error, null)
        }
    }

    /***************** Disable REST API *****************/

    Payment.disableRemoteMethod('create', true)
    Payment.disableRemoteMethod('find', true)
    Payment.disableRemoteMethod('findOne', true)
    Payment.disableRemoteMethod('findById', true)
    Payment.disableRemoteMethod('upsert', true)
    Payment.disableRemoteMethod('updateAll', true)
    Payment.disableRemoteMethod('deleteById', true)
    Payment.disableRemoteMethod('count', true)
    Payment.disableRemoteMethod('exists', true)
    Payment.disableRemoteMethod('createChangeStream', true)
    Payment.disableRemoteMethod('updateAttributes', false)
    Payment.disableRemoteMethod('replaceById', true)
    Payment.disableRemoteMethod('replaceOrCreate', true)
    Payment.disableRemoteMethod('upsertWithWhere', true)

    /***************** Remote methods *****************/

    Payment.remoteMethod(
        'finish',
        {
            description: 'Accept Veritrans Snap payment success redirection',
            accepts: [
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: { arg: 'body', type: 'object', root: true },
            http: {path: '/finish', verb: 'get'}
        }
    )

    Payment.remoteMethod(
        'unfinish',
        {
            description: 'Accept Veritrans Snap payment unfinished redirection',
            accepts: [
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: { arg: 'body', type: 'object', root: true },
            http: {path: '/unfinish', verb: 'get'}
        }
    )

    Payment.remoteMethod(
        'error',
        {
            description: 'Accept Veritrans Snap payment failed redirection',
            accepts: [
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: { arg: 'body', type: 'object', root: true },
            http: {path: '/error', verb: 'get'}
        }
    )

    Payment.remoteMethod(
        'notification',
        {
            description: 'Accept Veritrans Snap notification',
            accepts: [
                { arg: 'data', type: 'object', required: true, description: '{ status_code: string, status_message: string, transaction_id: string, masked_card: string, order_id: string, payment_type: string, transaction_time: string, transaction_status: string, fraud_status: string, approval_code: string, signature_key: string, bank: string, eci: string, gross_amount: string }', default: { status_code: "", status_message: "", transaction_id: "", masked_card: "", order_id: "", payment_type: "", transaction_time: "", transaction_status: "", fraud_status: "", approval_code: "", signature_key: "", bank: "", eci: "", gross_amount: "" }, http: { source: 'body' }},
                { arg: 'ctx', type: 'object', http: { source:'context' } }
            ],
            returns: { arg: 'body', type: 'object', root: true },
            http: {path: '/notification', verb: 'post'}
        }
    )

};
