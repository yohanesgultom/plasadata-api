module.exports = function(Cart) {

    var app = require('../../server/server')

    Cart.prototype.checkout = function (customer, cb) {
        var cart = this
        Cart.getObjectsByIds(cart.products, cart.bundles).then(function (objects) {
            // validate bundles
            [conflictBundles, products, bundles] = Cart.getConflictBundles(objects)
            if (Object.keys(conflictBundles).length > 0) {
                cb('Bundle(s) conflict', conflictBundles)
            } else {
                customer.purchase(products, bundles, function (err, purchases) {
                    if (err) {
                        cb(err, null)
                    } else {
                        // empty cart
                        cart.products = []
                        cart.bundles = []
                        cart.save(function(err, cart) {
                            if (err) {
                                cb(err, null)
                            } else {
                                cb(null, purchases)
                            }
                        })
                    }
                })
            }
        }, function (err) {
            cb(err, null)
        })
    }

    Cart.prototype.checkoutAsGift = function (customer, recipient, cb) {
        var cart = this
        Cart.getObjectsByIds(cart.products, cart.bundles).then(function (objects) {
            // validate bundles
            [conflictBundles, products, bundles] = Cart.getConflictBundles(objects)
            if (Object.keys(conflictBundles).length > 0) {
                cb('Bundle(s) conflict', conflictBundles)
            } else {
                customer.purchaseAsGift(recipient, products, bundles, function (err, purchases) {
                    if (err) {
                        cb(err, null)
                    } else {
                        // empty cart
                        cart.products = []
                        cart.bundles = []
                        cart.save(function(err, cart) {
                            if (err) {
                                cb(err, null)
                            } else {
                                cb(null, purchases)
                            }
                        })
                    }
                })
            }
        }, function (err) {
            cb(err, null)
        })
    }

    Cart.validate = function (products, bundles, cb) {
        Cart.getObjectsByIds(products, bundles).then(function (objects) {
            // validate bundles
            [conflictBundles, products, bundles] = Cart.getConflictBundles(objects)
            cb(null, conflictBundles)
        }, function (err) {
            cb(err, null)
        })
    }

    // get objects (products or bundles)
    // by list of ids
    // return a promise
    Cart.getObjectsByIds = function (productIds, bundleIds) {
        var promises = []
        if (productIds.length > 0) {
            promises.push(new Promise(function(resolve, reject) {
                // automatically handles product duplicates
                app.models.Product.find({where: {id: {inq: productIds}}}, function(err, p) {
                   if (err) {
                       reject(err)
                   } else {
                       resolve(p)
                   }
               })
            }))
        }
        if (bundleIds.length > 0) {
            promises.push(new Promise(function(resolve, reject) {
                // automatically handles bundle duplicates
                app.models.Bundle.find({where: {id: {inq: bundleIds}}}, function(err, b) {
                   if (err) {
                       reject(err)
                   } else {
                       resolve(b)
                   }
               })
            }))
        }
        return Promise.all(promises)
    }

    // get bundles that contains product(s) exist in list of products
    // objects composed of list of products and/or list of bundles
    Cart.getConflictBundles = function (objects) {
        var productIds = [],
            products = [],
            bundles = [],
            conflictBundles = {}

        // separate products and bundles
        objects.forEach(function(obj) {
            if (obj[0].constructor.definition.name == 'Product') {
                productIds = obj.map(function(p) {return p.id})
                products = obj
            } else if (obj[0].constructor.definition.name == 'Bundle') {
                bundles = obj
            }
        })

        bundles.forEach(function(b) {
            b.products().forEach(function(p) {
                if (productIds.indexOf(p.id) < 0) {
                    productIds.push(p.id)
                } else {
                    if (!conflictBundles[b.id]) {
                        conflictBundles[b.id] = []
                    }
                    conflictBundles[b.id].push(p.id)
                }
            })
        })

        return [conflictBundles, products, bundles]
    }

};
