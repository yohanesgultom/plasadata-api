module.exports = function(Bundle) {

    // Super important!
    // handle strange behaviour of decimal values
    Bundle.afterInitialize = function() {
        var fields = ['price', 'discount'],
        that = this
        fields.forEach(function(f) {
            if (typeof that[f] === 'string' || that[f] instanceof String) {
                that[f] = parseFloat(that[f])
            }
        })
    }

    // must be called separately
    // as currently no hook for this
    Bundle.prototype.setPrice = function() {
        var that = this
        return new Promise(function(resolve, reject) {
            that.products(function(err, products) {
                if (err) {
                    reject(err)
                } else {
                    // sum base price
                    // on assumption that bundle price
                    // are not affected by individual product's promotion(s)
                    that.price = 0.0
                    products.forEach(function(p) {
                        that.price += p.basePrice
                    })
                    // apply bundle discount
                    that.price = Math.round((that.price - (that.price * that.discount / 100)) * 100) / 100
                    that.save(function(err) {
                        if (err) {
                            reject(err)
                        } else {
                            resolve(that)
                        }
                    })
                }
            })
        })
    }

    Bundle.prototype.getExcerpt = function() {
        return {
            id: this.id,
            name: this.name,
            discount: this.discount
        }
    }

    /***************** Disable REST API *****************/

    Bundle.disableRemoteMethod('create', true)
    Bundle.disableRemoteMethod('upsert', true)
    Bundle.disableRemoteMethod('updateAll', true)
    Bundle.disableRemoteMethod('deleteById', true)
    Bundle.disableRemoteMethod('replaceOrCreate', true)
    Bundle.disableRemoteMethod('createChangeStream', true)
    Bundle.disableRemoteMethod('updateAttributes', false)
    Bundle.disableRemoteMethod('replaceById', true)
    Bundle.disableRemoteMethod('upsertWithWhere', true)
    Bundle.disableRemoteMethod('__create__products', false)
    Bundle.disableRemoteMethod('__upsert__products', false)
    Bundle.disableRemoteMethod('__delete__products', false)
    Bundle.disableRemoteMethod('__updateById__products', false)
    Bundle.disableRemoteMethod('__destroyById__products', false)
    Bundle.disableRemoteMethod('__link__products', false)
    Bundle.disableRemoteMethod('__unlink__products', false)

    /***************** Hooks *****************/

    Bundle.observe('access', function(ctx, next) {
        var now = new Date(),
            include = {
                relation: 'products'
            }
        if (ctx.query.include) {
            if (ctx.query.include.constructor === Array) {
                ctx.query.include.push(include)
            } else if (ctx.query.include != '') {
                ctx.query.include = [
                    {relation:ctx.query.include},
                    include
                ]
            } else {
                ctx.query.include = include
            }
        } else {
            ctx.query.include = include
        }
        next()
    })

}
