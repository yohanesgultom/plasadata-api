module.exports = function(Promotion) {

    var app = require('../../server/server')

    // Super important!
    // handle strange behaviour of decimal values
    Promotion.afterInitialize = function() {
        var fields = ['discount'],
        that = this
        fields.forEach(function(f) {
            if (typeof that[f] === 'string' || that[f] instanceof String) {
                that[f] = parseFloat(that[f])
            }
        })
    }

    Promotion.prototype.getExcerpt = function() {
        return {
            id: this.id,
            name: this.name,
            discount: this.discount
        }
    }

    /***************** Disable REST API *****************/

    Promotion.disableRemoteMethod("create", true)
    Promotion.disableRemoteMethod("upsert", true)
    Promotion.disableRemoteMethod("updateAll", true)
    Promotion.disableRemoteMethod("deleteById", true)
    Promotion.disableRemoteMethod("createChangeStream", true)
    Promotion.disableRemoteMethod("updateAttributes", false)
    Promotion.disableRemoteMethod('replaceById', true)
    Promotion.disableRemoteMethod('replaceOrCreate', true)
    Promotion.disableRemoteMethod('upsertWithWhere', true)
    Promotion.disableRemoteMethod('__create__products', false)
    Promotion.disableRemoteMethod('__upsert__products', false)
    Promotion.disableRemoteMethod('__delete__products', false)
    Promotion.disableRemoteMethod('__updateById__products', false)
    Promotion.disableRemoteMethod('__destroyById__products', false)
    Promotion.disableRemoteMethod('__link__products', false)
    Promotion.disableRemoteMethod('__unlink__products', false)

    /***************** Hooks *****************/

    // display only active (non expired) promo
    Promotion.beforeRemote('find', function(ctx, modelInstance, next) {
        var now = new Date(),
            defaultWhere = {
                startDate: {lt: now},
                endDate: {gt: now}
            },
            filter = ctx.args.filter ? JSON.parse(ctx.args.filter) : {}
        if (filter.where) {
            Object.keys(defaultWhere).forEach(function(key) {
                filter.where[key] = defaultWhere[key]
            })
        } else {
            filter['where'] = defaultWhere
        }
        ctx.args.filter = JSON.stringify(filter)
        next()
    })

    // remove empty promotions if products included
    Promotion.afterRemote('find', function(ctx, modelInstance, next) {
        var filter = ctx.args.filter ? JSON.parse(ctx.args.filter) : {}
        if (filter.include && filter.include == 'products') {
            if (ctx.result && ctx.result.length > 0) {
                var newResult = []
                for (var i = 0; i < ctx.result.length; i++) {
                    var products = ctx.result[i].products()
                    if (products && products.length > 0) {
                        newResult.push(ctx.result[i])
                    }
                }
                ctx.result = newResult
            }
        }
        next()
    })

    Promotion.afterRemote('prototype.__get__products', function(ctx, modelInstance, next) {
        app.models.Product.setPrice(ctx, function() {
            next()
        })
    })

    Promotion.afterRemote('prototype.__findById__products', function(ctx, modelInstance, next) {
        app.models.Product.setPrice(ctx, function() {
            next()
        })
    })

};
