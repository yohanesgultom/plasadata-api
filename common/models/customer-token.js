var assert = require('assert')

module.exports = function(Customertoken) {

    Customertoken.prototype.validate = function(cb) {
        try {
            assert(
                this.created && typeof this.created.getTime === 'function',
                'token.created must be a valid Date'
            )
            assert(this.ttl !== 0, 'token.ttl must be not be 0')
            assert(this.ttl, 'token.ttl must exist')

            var now = Date.now()
            var created = this.created.getTime()
            var elapsedSeconds = (now - created) / 1000
            var secondsToLive = this.ttl
            // eternal token
            var isValid = secondsToLive < 0 || elapsedSeconds < secondsToLive

            if (isValid) {
                cb(null, isValid)
            } else {
                this.destroy(function(err) {
                    cb(err, isValid)
                })
            }
        } catch (e) {
            cb(e)
        }
    }
}
