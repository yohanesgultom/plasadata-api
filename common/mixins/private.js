module.exports = function(Model, options) {
    'use strict'

    var app = require('../../server/server')

    function sanitize(body, fields) {
        fields.forEach(function(field) {
            delete body[field]
        })
        return body
    }

    Model.beforeRemote('create', function( ctx, modelInstance, next) {
        ctx.req.body = sanitize(ctx.req.body, options.fields)
        next()
    })

    Model.afterRemote('upsert', function( ctx, modelInstance, next) {
        ctx.req.body = sanitize(ctx.req.body, options.fields)
        next()
    })

};
