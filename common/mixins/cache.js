module.exports = function(Model, options) {
    'use strict'

    var app = require('../../server/server')

    function isObj(obj) {
        return obj === Object(obj);
    }

    function isArray(obj) {
        return Object.prototype.toString.call(obj) === '[object Array]'
    }

    function flattenObject(obj) {
        var list = []
        for (var key in obj) {
            var val = obj[key]
            if (!val) {
                // skip
            } else if (isArray(val)) {
                list.push(key + '=' + val.join(','))
            } else if (isObj(val)) {
                list.push(key + '=[' + flattenObject(val) + ']')
            } else {
                list.push(key + '=' + val)
            }
        }
        return (list && list.length > 0) ? list.join('_') : ''
    }

    function getCacheKey(methodName, args) {
        var components = [],
            args = preProcessParams(args)
        components.push(methodName)
        var flattenArgs = flattenObject(args)
        if (flattenArgs != '') {
            components.push(flattenArgs)
        }
        return components.join('_')
    }

    function preProcessParams(args) {
        var res = {}
        // remove circular reference
        for (var key in args) {
            if (key == 'filter') {
                var filter = JSON.parse(args[key])
                for (var fkey in filter) {
                    res[fkey] = filter[fkey]
                }
            } else if (key != 'ctx') {
                res[key] = args[key]
            }
        }
        return res
    }

    Model.beforeRemote('**', function( ctx, modelInstance, next) {
        var methodString = ctx.methodString.replace(Model.definition.name + '.', '')
        if (Object.keys(options).indexOf(methodString) > -1) {
            var cacheKey = getCacheKey(ctx.methodString, ctx.args)
            ctx.hookState = {cacheKey: cacheKey}
            app.models.Cache.findOne({where: {key: cacheKey}}, function (err, cache) {
                if (cache) {
                    var expiredTime = cache.createdAt.getTime() + cache.ttl * 1000,
                        currentTime = new Date().getTime()
                    // ttl = 0 means eternal
                    if (cache.ttl != 0 && currentTime >= expiredTime) {
                        // evict cache
                        app.models.Cache.destroyAll({key: cacheKey}, function(err) {
                            next()
                        })
                    } else {
                        // load from cache
                        ctx.hookState.cached = true
                        ctx.res.send(cache.value)
                    }
                } else {
                    next()
                }
            })
        } else {
            next()
        }
    })

    Model.afterRemote('**', function( ctx, modelInstance, next) {
        var methodString = ctx.methodString.replace(Model.definition.name + '.', '')
        if (Object.keys(options).indexOf(methodString) > -1) {
            // if loaded from cache, no need to save again
            if (ctx.hookState.cached == true) {
                next()
            } else {
                var cacheKey = ctx.hookState.cacheKey,
                    newCache = {key: cacheKey, value: ctx.result}

                // if available get ttl from options
                if (options[methodString].hasOwnProperty('ttl')) {
                    newCache.ttl = options[methodString].ttl
                }

                app.models.Cache.create(newCache, function (err) {
                    next()
                })
            }
        } else {
            next()
        }
    })

};
