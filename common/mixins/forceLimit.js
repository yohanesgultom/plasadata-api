var MAX_QUERY = 100

module.exports = function(Model, options) {
    'use strict'

    var limit = options.limit ? options.limit : MAX_QUERY

    Model.beforeRemote('find', function(ctx, modelInstance, next) {
        // force limit to prevent server overload
        if (!ctx.args.filter) {
            ctx.args.filter = JSON.stringify({limit: limit})
        } else {
            var filter = JSON.parse(ctx.args.filter)
            filter.limit = filter.limit > limit ? limit : filter.limit
            ctx.args.filter = JSON.stringify(filter)
        }
        next()
    })

};
