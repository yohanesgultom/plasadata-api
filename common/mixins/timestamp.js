module.exports = function(Model, options) {
    'use strict'

    var app = require('../../server/server')

    Model.observe('before save', function (ctx, next) {
        if (ctx.isNewInstance) {
            ctx.instance.createdAt = new Date()
        } else if (ctx.data) {
            ctx.data.updatedAt = new Date()
        }
        next()
    })

    Model.afterRemote('create', function( ctx, modelInstance, next) {
        app.models.Customer.findById(ctx.req.accessToken.userId, function(err, user) {
            modelInstance.createdBy = user.username
            next()
        })
    })

    Model.afterRemote('upsert', function( ctx, modelInstance, next) {
        app.models.Customer.findById(ctx.req.accessToken.userId, function(err, user) {
            modelInstance.updatedBy = user.username
            next()
        })
    })

    Model.afterRemote('*.updateAttributes', function( ctx, modelInstance, next) {
        app.models.Customer.findById(ctx.req.accessToken.userId, function(err, user) {
            modelInstance.updatedBy = user.username
            next()
        })
    })

};
