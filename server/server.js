var loopback = require('loopback');
var boot = require('loopback-boot');
var _ = require('lodash');
var app = module.exports = loopback();
var server;

// email verification
// NOTE: somehow it only works with EJS (doesn't work with PUG)
var path = require('path')
var bodyParser = require('body-parser')
app.set('view engine', 'ejs');
// email verification response
app.get('/verified', function(req, res) {
    res.status(204).send()
})

// workaround for swagger 2.0 spec that doesn't accept null value
// https://github.com/strongloop/loopback-component-explorer/issues/146
app.get('/explorer/swagger.json', function (req, res, next) {
    var _send = res.send;
    res.send = function (data) {
        if (data && data.definitions) {
            _.forOwn(data.definitions, function (model) {
                _.forOwn(model.properties, function (prop) {
                    if (prop.default === null) {
                        delete prop.default;
                    }
                });
            });
        }
        _send.apply(res, arguments);
    };
    next();
});

app.start = function() {
    // start the web server
    return app.listen(function() {
        app.emit('started');
        var baseUrl = app.get('url').replace(/\/$/, '');
        console.log('Web server listening at: %s', baseUrl);
        if (app.get('loopback-component-explorer')) {
            var explorerPath = app.get('loopback-component-explorer').mountPath;
            console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
        }
    });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
    if (err) throw err;
    // start the server if `$ node server.js`
    if (require.main === module)
    server = app.start();
});
