// Initialize databases with required data to start
module.exports = function(app, cb) {
    var postgresDS = app.dataSources.postgresDS

    postgresDS.once('connected', function() {
        postgresDS.automigrate().then(function() {
            createUserRoles(function(err, roles) {
                if (err) throw err
                createAdmins(roles, function(err, admins) {
                    if (err) throw err
                    if (!admins || admins.length <= 0) throw 'No admins created'
                    app.emit('init-app-data')
                    console.log('\x1b[32m%s\x1b[0m', 'Initialized with core data')
                    process.nextTick(cb)
                })
            })
        }, function (err) { cb(err) })
    })

    function createUserRoles(cb) {
        postgresDS.automigrate('Role', function(err) {
            if (err) return cb(err)
            app.models.Role.create([
                {name: 'admin'},
                {name: 'buyer'},
                {name: 'seller'},
            ], cb)
        })
    }

    function createAdmins(roles, cb) {
        app.models.Customer.create([
            {username: 'admin', firstName: 'admin', email: 'admin@plasadata.ga', password: 'admin123', emailVerified: true}
        ], function(err, customers) {
            roles.forEach(function(role) {
                if (role.name == 'admin') {
                    role.principals.create({
                        principalType: app.models.RoleMapping.USER,
                        principalId: customers[0].id
                    })
                }
            })
            if (cb) cb(err, customers)
        })
    }

}
