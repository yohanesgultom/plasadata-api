module.exports = function(app, cb) {
    // constraints
    app.dataSources.storage.connector.allowedContentTypes = ["image/jpg", "image/jpeg", "image/png"]
    app.dataSources.storage.connector.maxFileSize = 5 * 1024 * 1024

    // rename uploaded file
    app.dataSources.storage.connector.getFilename = function(file, req, res){
        var userId = req.accessToken.userId,
            container = file.container,
            time = new Date().getTime(),
            name = userId + '_' + time + '_' + file.name

        if (container == 'products') {
            name = req.params.id + '_' + file.name
        }

        return name
    }

    process.nextTick(cb)
}
