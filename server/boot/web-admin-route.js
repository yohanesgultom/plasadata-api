/* Web admin routing */
var path = require('path'),
    express = require('express'),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    request = require('request')

const VIEWS_DIR = path.join(__dirname, '..', '..', 'web'),
    ASSETS_DIR = path.join(__dirname, '..', '..', 'web', 'assets')

// track uptime
const started = new Date()

module.exports = function(app, cb) {
    const VERITRANS = app.models.Customer.getVeritransProperties()

    app.set('view engine', 'pug')
    app.set('views', VIEWS_DIR)
    app.use(express.static(ASSETS_DIR))
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(cookieParser())
    app.use(session({
        secret: '2C99-4X5A-A2lQ99X',
        resave: true,
        saveUninitialized: true
    }))

    // Authentication and Authorization Middleware
    var auth = function(req, res, next) {
        if (req.session && req.session.token && req.session.username && req.session.roles) {
            context = req.path.split('/')[1]
            if (req.session.roles && req.session.roles.indexOf(context) > -1) {
                return next()
            } else {
                req.session.redirect_to = req.path
                res.status(401).redirect('/401')
            }
        } else {
            req.session.redirect_to = req.path
            res.status(401).redirect('/auth/login')
        }
    }

    app.get('/', function(req, res) {
        var params = {}
        params.status = {
            started: started,
            uptime: (Date.now() - Number(started)) / 1000
        }
        res.render('index', params)
    })

    // Not authorized
    app.get('/401', function(req, res) {
        res.render('401')
    })

    // Login page
    app.get('/auth/login', function(req, res) {
        var params = {}
        if (req.query.error) params.error = req.query.error
        res.render('login', params)
    })

    // Login endpoint
    app.post('/auth/signin', function (req, res) {
        var error
        // authenticate against database
        app.models.Customer.login({username: req.body.username, password: req.body.password}, 'user', function (err, token) {
            if (err) {
                error = encodeURIComponent(err.toString())
                res.redirect('/auth/login?error=' + error)
            } else if (!token || !token.id) {
                error = encodeURIComponent('invalid username/password')
                res.redirect('/auth/login?error=' + error)
            } else {
                // check roles
                // and allows only admin
                token.__data.user.roles({}, function(err, roles) {
                    if (err) {
                        error = encodeURIComponent(err.toString())
                        res.redirect('/auth/login?error=' + error)
                    } else if (!roles || roles.length <= 0) {
                        error = encodeURIComponent('only admins are allowed')
                        res.redirect('/auth/login?error=' + error)
                    } else {
                        // set variables to session
                        req.session.token = token.id
                        req.session.username = token.__data.user.username
                        req.session.userid = parseInt(token.__data.user.id)
                        req.session.roles = []
                        for (var i = 0; i < roles.length; i++) {
                            req.session.roles.push(roles[i].name)
                        }
                        // redirect to authenticated area
                        if (req.session.redirect_to) {
                            res.redirect(req.session.redirect_to)
                        } else {
                            if (req.session.roles.indexOf('admin') > -1) {
                                res.redirect('/admin')
                            } else {
                                // TODO handle more roles here
                                res.redirect('/buyer')
                            }
                        }
                    }
                })
            }
        })
    })

    // Logout endpoint
    app.get('/auth/logout', function (req, res) {
        req.session.destroy()
        res.status(204).redirect('/auth/login')
    })

    // Secured area
    app.get('/admin', auth, function (req, res) {
        var params = {}
        app.models.Product.find({}, function(err, products) {
            if (err) params.error = err.toString()
            if (products) params.products = products
            res.render('admin/index', params)
        })
    })

    // Secured area
    app.get('/buyer', auth, function (req, res) {
        var baseUrl = app.get('url').replace(/\/$/, ''),
            url = baseUrl + '/api/Customers/' + req.session.userid + '?access_token=' + req.session.token,
            headers = {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }

        console.log('POST ' + url)
        // get customer profile
        request.get({url: url, headers: headers}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : {}
            if (err) {
                console.error(err)
                body.error = err
            } else if (body.error) {
                console.error(body.error)
                body.error = body.error.message
            } else {
                body = {customer: body}
                if (req.query && req.query.error) {
                    body.error = req.query.error
                }
            }
            res.render('buyer/index', body)
        })
    })

    app.get('/buyer/payment', auth, function (req, res) {
        var baseUrl = app.get('url').replace(/\/$/, ''),
            url = baseUrl + '/api/Customers/' + req.session.userid + '/topup?access_token=' + req.session.token,
            headers = {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body = {},
            params = {}

        try {
            console.log(req.query)
            body.amount = parseFloat(req.query.topup_amount)
            console.log('POST ' + url)
            request.post({url: url, headers: headers, body: JSON.stringify(body)}, function (err, httpResponse, body) {
                body = body ? JSON.parse(body) : {}
                if (err) {
                    console.error(err)
                    body.error = err
                } else if (body.error) {
                    console.error(body.error)
                    body.error = body.error.message
                }
                res.render('buyer/payment', body)
            })
        } catch (err) {
            res.redirect('buyer/index?error=' + err)
        }
    })

    app.get('/buyer/streaming', auth, function (req, res) {
        var params = {},
            customerId = req.session.userid,
            token = req.session.token
        app.models.Purchase.find({where: {customerId: customerId}}, function (err, purchases) {
            if (err) {
                params.error = err
            } else if (!purchases || purchases.length <= 0) {
                params.error = 'No purchased product'
            } else {
                for (var i = 0; i < purchases.length; i++) {
                    if (purchases[i].productTags.indexOf('video') > -1) {
                        params.video = '/api/Customers/' + customerId + '/products/' + purchases[i].productId + '/download?access_token=' + req.session.token
                        if (params.audio) break
                    } else if (purchases[i].productTags.indexOf('music') > -1) {
                        params.audio = '/api/Customers/' + customerId + '/products/' + purchases[i].productId + '/download?access_token=' + req.session.token
                        if (params.video) break
                    }
                }
            }
            res.render('buyer/streaming', params)
        })

    })

    process.nextTick(cb)
}
