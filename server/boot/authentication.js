module.exports = function enableAuthentication(server, cb) {
    // enable authentication
    server.enableAuth()
    process.nextTick(cb)
}
