// init job scheduler
module.exports = function(app, cb) {
    var later = require('later')

    later.date.localTime()

    /**** define job scheduling below ****/

    // TODO uncomment to enable
    // later.setInterval(
    //     app.models.Message.createSubscriptionMessages,
    //     later.parse.recur().on(6).hour()
    // )

    console.log('\x1b[32m%s\x1b[0m', 'Scheduler initialized ')
    process.nextTick(cb)
}
