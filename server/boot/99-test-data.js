// Init database with dummy data
var fs = require('fs'),
    path = require('path'),
    moment = require('moment'),
    faker = require('faker'),
    parse = require('csv-parse'),
    async = require('async')

const tags = {
        "music": {
            "rock": {},
            "pop": {},
            "classic": {},
            "electronic": {},
            "jazz": {},
            "rnb": {},
        },
        "video": {
            "movie": {},
            "documentary": {},
            "tutorial": {},
            "series": {},
        },
        "ebook": {
            "guide": {},
            "magazine": {},
            "textbook": {},
            "novel": {},
            "comic": {},
        }
    },
    subscriptionTags = {
        "subscription": {
            "subscription-music": {},
            "subscription-video": {},
            "subscription-ebook": {},
        }
    },
    durations = ['7 d', '1 M', '1 y'], // for subscriptions
    creator = 'init-app-data',
    IMAGE_API_URL = '/api/images/products/download/',
    PROMO_IMAGE_API_URL = '/api/images/promotions/download/',
    TEST_DIR = path.join(__dirname, '..', '..', 'test', 'resources'),
    PRODUCTS_IMAGE_DIR = path.join(__dirname, '..', '..', 'storage', 'products'),
    PROMO_IMAGE_DIR = path.join(__dirname, '..', '..', 'storage', 'promotions'),
    CONTENTS_DIR = path.join(__dirname, '..', '..', 'contents', 'files'),
    SAMPLES_DIR = path.join(__dirname, '..', '..', 'contents', 'samples'),
    DATA_CSV = path.join(TEST_DIR, 'data.csv'),
    now = new Date(),
    URLS = copyFiles()

function copyFiles() {

    // images
    fs.createReadStream(path.join(TEST_DIR, 'promotion.png')).pipe(fs.createWriteStream(path.join(PROMO_IMAGE_DIR, 'promotion.png')))
    var files = fs.readdirSync(TEST_DIR);
    for(var i = 0; i < files.length; i++){
        var ext = path.extname(files[i])
        if (['.jpg', '.png'].indexOf(ext) > -1) {
            var basename = path.basename(files[i])
            fs.createReadStream(path.join(TEST_DIR, basename)).pipe(fs.createWriteStream(path.join(PRODUCTS_IMAGE_DIR, basename)))
        }
    }
     // contents
    fs.createReadStream(path.join(TEST_DIR, 'test.mp3')).pipe(fs.createWriteStream(path.join(CONTENTS_DIR, 'test.mp3')))
    fs.createReadStream(path.join(TEST_DIR, 'test.mp4')).pipe(fs.createWriteStream(path.join(CONTENTS_DIR, 'test.mp4')))
    fs.createReadStream(path.join(TEST_DIR, 'test.pdf')).pipe(fs.createWriteStream(path.join(CONTENTS_DIR, 'test.pdf')))
    // samples
    fs.createReadStream(path.join(TEST_DIR, 'test_sample.mp3')).pipe(fs.createWriteStream(path.join(SAMPLES_DIR, 'test_sample.mp3')))
    fs.createReadStream(path.join(TEST_DIR, 'test_sample.mp4')).pipe(fs.createWriteStream(path.join(SAMPLES_DIR, 'test_sample.mp4')))
    fs.createReadStream(path.join(TEST_DIR, 'test_sample.pdf')).pipe(fs.createWriteStream(path.join(SAMPLES_DIR, 'test_sample.pdf')))

    return {
        image: {
            music: IMAGE_API_URL + 'music.jpg',
            video: IMAGE_API_URL + 'video.jpg',
            ebook: IMAGE_API_URL + 'ebook.jpg',
            banner: IMAGE_API_URL + 'banner.png',
            promotion: PROMO_IMAGE_API_URL + 'promotion.png',
        },
        content: {
            music: 'test.mp3',
            video: 'test.mp4',
            ebook: 'test.pdf',
        },
        sample: {
            music: 'test_sample.mp3',
            video: 'test_sample.mp4',
            ebook: 'test_sample.pdf',
        }
    }
}

module.exports = function(app, cb) {
    // data sources
    var mongoDS = app.dataSources.mongoDS

    mongoDS.automigrate().then(function(error) {
        if (error) throw error
        if (fs.existsSync(DATA_CSV)) {
            loadDummyData(DATA_CSV, function(err) {
                if (err) throw err
                console.log('\x1b[32m%s\x1b[0m', 'Initialized with loaded dummy data from CSV')
                process.nextTick(cb)
            })
        } else {
            generateDummyData(function(err, res) {
                if (err) throw err
                console.log('\x1b[32m%s\x1b[0m', 'Initialized with generated dummy data')
                process.nextTick(cb)
            })
        }
    })

    /* FUNCTIONS */

    function loadDummyData(csvFile, cb) {
        fs.readFile(csvFile, function (err, data) {
            parse(data, {trim: true, from: 2, delimiter: '\t'}, function(err, rows) {
                var promises = []
                rows.forEach(function(line) {
                    var albumName = line[0].trim(),
                        albumDesc = line[1].trim() ? line[1].trim() : faker.lorem.paragraph(),
                        prodName = line[2].trim() ? line[2].trim() : albumName,
                        prodDesc = line[3].trim() ? line[3].trim() : faker.lorem.paragraph(),
                        prodPrice = line[4].trim() ? parseInt(line[4].trim()) : faker.commerce.price(),
                        tag = line[5].trim() ? line[5].trim() : faker.helpers.randomize(Object.keys(tags)),
                        albumPrimaryImage = line[6].trim() ? IMAGE_API_URL + line[6].trim() : URLS.image[tag]

                    if (albumName) {
                        promises.push(new Promise(function(resolve, reject) {
                            app.models.Album.create({
                                name: albumName,
                                desc: albumDesc,
                                tags: [tag],
                                primaryImage: albumPrimaryImage
                            }, function (err, album) {
                                if (err) reject(err)
                                album.products.create({
                                    name: prodName,
                                    desc: prodDesc,
                                    basePrice: prodPrice,
                                    tags: [tag],
                                    primaryImage: albumPrimaryImage,
                                    secondaryImage: URLS.image.banner,
                                    file: URLS.content[tag],
                                    sample: URLS.sample[tag],
                                    createdBy: creator
                                }, function(err, prod) {
                                    if (err) reject(err)
                                    resolve(album)
                                })
                            })
                        }))
                    }
                })

                Promise.all(promises).then(function() {
                    createCustomers(function(err, customers) {
                        if (err) throw err
                        makePurchases(customers[0], ['music'], function(err, purchases) {
                            if (err) throw err
                            makePurchases(customers[0], ['video'], cb, 1)
                        }, 1)
                    })
                })

            })
        })
    }

    function generateDummyData(cb) {
        createProducts(function(err, products) {
            if (err) throw err
            createSubscriptions(function(err, subscriptions) {
                if (err) throw err
                createPromotions(products, function(err, promotions) {
                    if (err) throw err
                    createCustomers(function(err, customers) {
                        if (err) throw err
                        makePurchases(customers[0], ['ebook', 'music'], function(err, purchases) {
                            if (err) throw err
                            makePurchases(customers[0], ['video'], function(err, purchases) {
                                if (err) throw err
                                addMessages(customers[0], function(err, messages) {
                                    if (err) throw err
                                    createBundles(products, function(err, bundles) {
                                        if (err) throw err
                                        createAlbums(20, cb)
                                    })
                                })
                            }, 3)
                        })
                    })
                })
            })
        })
    }

    function createCustomers(cb) {
        app.models.Customer.create([
            {username: 'buyer', firstName: 'buyer', email: 'buyer@gmail.com', password: 'buyer123', balance: 1000000, emailVerified: true},
            {username: 'buyer2', firstName: 'buyer2', email: 'buyer2@gmail.com', password: 'buyer123', balance: 2000000, emailVerified: true},
            {username: 'seller', firstName: 'seller', email: 'seller@gmail.com', password: 'seller123', emailVerified: true},
            {username: 'buyer3', firstName: 'buyer3', email: 'buyer3@gmail.com', password: 'buyer123', balance: 3000000, emailVerified: true},
            {username: 'buyer4', firstName: 'buyer4', email: 'buyer4@gmail.com', password: 'buyer123', balance: 3000000, emailVerified: true},
            {username: 'buyer5', firstName: 'buyer5', email: 'buyer5@gmail.com', password: 'buyer123', balance: 3000000, emailVerified: true},
            {username: 'buyer6', firstName: 'buyer6', email: 'buyer6@gmail.com', password: 'buyer123', balance: 3000000, emailVerified: true},
        ], function(err, customers) {
            if (err) throw(err)
            getRoles(function(roles) {
                roles.forEach(function(role) {
                    if (role.name == 'buyer') {
                        role.principals.create([
                            {
                                principalType: app.models.RoleMapping.USER,
                                principalId: customers[0].id
                            },
                            {
                                principalType: app.models.RoleMapping.USER,
                                principalId: customers[1].id
                            },
                            {
                                principalType: app.models.RoleMapping.USER,
                                principalId: customers[3].id
                            },
                            {
                                principalType: app.models.RoleMapping.USER,
                                principalId: customers[4].id
                            },
                            {
                                principalType: app.models.RoleMapping.USER,
                                principalId: customers[5].id
                            },
                            {
                                principalType: app.models.RoleMapping.USER,
                                principalId: customers[6].id
                            },
                        ])
                    } else if (role.name == 'seller') {
                        role.principals.create({
                            principalType: app.models.RoleMapping.USER,
                            principalId: customers[2].id
                        })
                    }
                })
                if (cb) cb(err, customers)
            })
        })
    }

    function getRoles(cb) {
        app.models.Role.find(function(err, roles) {
            if (roles && roles.length > 0) {
                cb(roles)
            } else {
                setTimeout(function(){ getRoles(cb) }, 1000)
            }
        })
    }

    function createProducts(count, cb) {
        if (!cb && typeof(count) == 'function') {
            cb = count
            count = 100
        }
        mongoDS.automigrate('Product', function(err) {
            if (err) return cb(err)
            var newProducts = []
            for (var i = 0; i < count; i++) {
                var parentTag = faker.helpers.randomize(Object.keys(tags)),
                    secondaryTag = faker.helpers.randomize(Object.keys(tags[parentTag]))
                newProducts.push({
                    name: faker.lorem.words(),
                    desc: faker.lorem.paragraphs(),
                    tags: [parentTag, secondaryTag],
                    basePrice: randFloat(5000, 50000),
                    primaryImage: URLS.image[parentTag],
                    secondaryImage: URLS.image.banner,
                    file: URLS.content[parentTag],
                    sample: URLS.sample[parentTag],
                    createdBy: creator
                })
            }
            app.models.Product.create(newProducts, cb)
        })
    }

    function makePurchases(buyer, tags, cb, limit) {
        limit = limit ? limit : 10
        app.models.Product.find({where: {tags: {inq: tags}}, limit: limit}, function(err, products) {
            if (err) throw err
            if (!products || products.length <= 0) throw 'No product found'
            buyer.purchase(products, [], cb)
        })
    }

    function _createPromotions(products, cb) {
        app.models.Promotion.create([
            {
                name: 'Promo this week',
                startDate: now,
                endDate: moment(now).add(7, 'days'),
                discount: 50,
                primaryImage: URLS.image.promotion
            },
            {
                name: 'Hot sales',
                startDate: now,
                endDate: moment(now).add(1, 'months'),
                discount: 25,
                primaryImage: URLS.image.promotion
            },
            {
                name: 'Hot promotions',
                startDate: now,
                endDate: moment(now).add(2, 'months'),
                discount: 10,
                primaryImage: URLS.image.promotion
            },
            {
                name: 'Expired promo',
                startDate: moment(now).subtract(2, 'months'),
                endDate: moment(now).subtract(1, 'months'),
                discount: 90,
                primaryImage: URLS.image.promotion
            }
        ], function(err, promotions) {
            if (err) throw err
            // add some products to promotions
            var productGroups = [],
                promises = [],
                productsPerGroup = Math.ceil(products.length / promotions.length / 3),
                prodIndex = 0

            // distribute products to promo groups
            for (var i = 0; i < promotions.length; i++) {
                if (productGroups.length <= i) productGroups.push([])
                var count = 0
                while (count < productsPerGroup && prodIndex < products.length) {
                    productGroups[i].push(products[prodIndex])
                    prodIndex++
                    count++
                }
            }

            // add one product with double active promo
            productGroups[0].push(faker.helpers.randomize(productGroups[1]))

            // add product groups to corresponding promo
            for (var i=0; i < promotions.length; i++) {
                productGroups[i].forEach(function(prod) {
                    promises.push(new Promise(function (resolve, reject) {
                        promotions[i].products.add(prod, function(err, added) {
                            if (err) {
                                reject(err)
                            } else {
                                resolve(added)
                            }
                        })
                    }))
                })
            }

            // wait until all products added
            Promise.all(promises).then(function (values) {
                cb(null, promotions)
            }, function (error) {
                cb(error, null)
            })
        })
    }

    function createPromotions(products, cb) {
        app.models.Promotion.findOne(function(err, promo) {
            if (err) {
                setTimeout(function() { createPromotions(products, cb) }, 1000)
            } else {
                _createPromotions(products, cb)
            }
        })
    }

    function rand(min, max) {
        return Math.round((Math.random() * max) + min)
    }

    function randFloat(min, max, decimals) {
        decimals = decimals ? decimals : 2
        var c = Math.pow(10, decimals)
        return Math.round((Math.random() * max + min) * c) / c
    }

    function addMessages(buyer, cb) {
        var messages = []
        buyer.products({}, function (err, products) {
            var excerpts = products.map(function(p) {
                return p.getExcerpt(buyer.id)
            })
            for (var i = 0; i < 10; i++) {
                var count = rand(0, products.length),
                    attachments = faker.helpers.shuffle(excerpts).slice(0, count)
                messages.push({
                    subject: faker.lorem.sentence(),
                    body: faker.lorem.paragraph(),
                    attachments: attachments.length > 0 ? attachments : null
                })
            }
            buyer.messages.create(messages, cb)
        })

    }

    function getRandomIntArrayUniqueElements(n, min, max) {
        var randomArray = [],
            randInt = rand(min, max)
        for (var i = 0; i < n; i++) {
            while (randomArray.indexOf(randInt) > -1) {
                randInt = rand(min, max)
            }
            randomArray.push(randInt)
        }
        return randomArray
    }

    function createAlbums(count, cb) {
        var newAlbums = []

        // create some albums
        for (var i = 0; i < count; i++) {
            var parentTag = faker.helpers.randomize(Object.keys(tags))
            newAlbums.push({
                name: faker.lorem.words(),
                primaryImage: URLS.image[parentTag],
                tags:[parentTag]
            })
        }

        // persist albums and add products to them
        app.models.Album.create(newAlbums, function(err, albums) {
            var promises = []
            if (err) cb(err, null)
            albums.forEach(function(album) {
                var albumSize = rand(1, 5),
                    parentTag = album.tags[0],
                    secondaryTag = faker.helpers.randomize(Object.keys(tags[parentTag])),
                    p = Promise.resolve()

                for (var i = 0; i < albumSize; i++) {
                    p = p.then(function() {
                        return new Promise(function (resolve, reject) {
                            album.products.create({
                                name: faker.lorem.words(),
                                desc: faker.lorem.paragraphs(),
                                tags: [parentTag, secondaryTag],
                                basePrice: randFloat(5000, 50000),
                                primaryImage: URLS.image[parentTag],
                                secondaryImage: URLS.image.banner,
                                file: URLS.content[parentTag],
                                sample: URLS.sample[parentTag],
                                createdBy: creator
                            }, function (err, product) {
                                if (err) {
                                    reject(err)
                                } else {
                                    resolve(product)
                                }
                            })

                        })
                    })
                }
                promises.push(p)
            })

            Promise.all(promises).then(function(products) {
                cb(null, albums)
            }, function (err) {
                cb(err, null)
            })

        })

    }

    function createBundles(products, cb) {
        var newBundles = [],
            count = Math.round(products.length * 0.1)

        // create some bundles
        for (var i = 0; i < count; i++) {
            newBundles.push({
                name: faker.lorem.words(),
                discount: rand(10, 80),
                startDate: now,
                endDate: moment(now).add(1, 'months'),
                createdBy: creator
            })
        }

        // persist bundles and add products to them
        app.models.Bundle.create(newBundles, function(err, bundles) {
            var promises = []
            if (err) cb(err, null)
            bundles.forEach(function(bundle) {
                var bundleSize = rand(1, 5),
                    randomIndexes = getRandomIntArrayUniqueElements(bundleSize, 0, products.length)

                for (var i = 0; i < bundleSize; i++) {
                    promises.push(new Promise(function(resolve, reject) {
                        var product = products[randomIndexes[i]]
                        bundle.products.add(product, function (err) {
                            if (err) {
                                reject(err)
                            } else {
                                resolve(product)
                            }
                        })
                    }))
                }
            })
            // update bundle prices
            Promise.all(promises).then(function(product) {
                var savePromises = []
                bundles.forEach(function (b) {
                    savePromises.push(b.setPrice())
                })
                Promise.all(savePromises).then(function(bundles) {
                    cb(null, bundles)
                }, function (err) {
                    cb(err)
                })
            }, cb)
        })

    }

    function createSubscriptions(count, cb) {
        if (!cb && typeof(count) == 'function') {
            cb = count
            count = 100
        }
        var newSubscriptions = []
        for (var i = 0; i < count; i++) {
            var parentTag = Object.keys(subscriptionTags)[0],
                secondaryTag = faker.helpers.randomize(Object.keys(subscriptionTags[parentTag]))
            newSubscriptions.push({
                name: 'Subscription of ' + faker.lorem.words(),
                desc: faker.lorem.paragraphs(),
                tags: [parentTag, secondaryTag],
                basePrice: randFloat(10000, 100000),
                primaryImage: URLS.image[secondaryTag.split('-')[1]],
                createdBy: creator,
                duration: faker.helpers.randomize(durations) // moment.js friendly format
            })
        }
        // create subscriptions
        app.models.Product.create(newSubscriptions, function (err, subscriptions) {
            if (err) throw err
            var promises = []
            // create subscription items
            subscriptions.forEach(function(s) {
                var randCount = rand(1, 5),
                    newItems = []
                for (var i = 0; i < randCount; i++) {
                    promises.push(new Promise(function(resolve, reject) {
                        s.subscriptionItems.create({
                            name: s.name + ' EP ' + i,
                            file: URLS.content[s.tags[1].split('-')[1]],
                            publishDate: moment(now).add(i, 'd')
                        }, function (err, items) {
                            if (err) {
                                reject(err)
                            } else {
                                resolve(items)
                            }
                        })
                    }))
                }
            })
            Promise.all(promises).then(function(values) {
                cb(null, values)
            }, function (err) {
                cb(err)
            })
        })
    }

} // end of module exports
