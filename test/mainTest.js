var chai = require('chai'),
    chaiHttp = require('chai-http'),
    app = require('../server/server')

chai.use(chaiHttp)

var resetDatabase = (app, cb) => {
    app.dataSources.mongoDS.automigrate().then(() => {
        app.dataSources.postgresDS.automigrate().then(cb)
    })
}

var init = (app, done) => {
    app.once('started', () => {
        resetDatabase(app, () => { done() })
    })
    app.start()
}

before('Initialize server', (done) => {
    if (app.booting) {
        // if boot script(s) takes time
        app.once('booted', () => { init(app, done) })
    } else {
        // if boot script(s) is fast
        init(app, done)
    }
})

require('./specs/auth')(app, resetDatabase)
require('./specs/album')(app, resetDatabase)
require('./specs/customer')(app, resetDatabase)
