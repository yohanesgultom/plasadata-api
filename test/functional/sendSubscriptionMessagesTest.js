var app = require('../../server/server.js'),
    moment = require('moment'),
    start = moment().startOf('day'),
    end = moment().endOf('day')

getCustomer('buyer2')

.then(function (args) {
    [customer] = args
    return new Promise(function (resolve, reject) {
        app.models.SubscriptionItem.find({where: {publishDate: {between: [start, end]}}, include: 'subscription', limit: 3}, function (err, items) {
            if (err || !items) {
                console.log(err)
                getSubscriptionByPublishDate(start, end).then(resolve, reject)
            } else {
                resolve([customer, items])
            }
        })
    })
}, onError)

.then(function (args) {
    [customer, items] = args
    subscriptions = items.map(function (i) { return i.subscription() })
    return new Promise(function (resolve, reject) {
        customer.purchase(subscriptions, [], function (err, purchases) {
            if (err) {
                reject(err)
            } else {
                resolve([customer, items, purchases])
            }
        })
    })
}, onError)

.then(function (args) {
    [customer, items, purchases] = args
    return new Promise(function (resolve, reject) {
        app.models.Message.createSubscriptionMessages(null, function (err, messages) {
            if (err) {
                reject(err)
            } else if (!messages || messages.length < 3)  {
                reject('Wrong number of messages created')
            } else {
                messages.forEach(function(msg) {
                    if (!('attachments' in msg)) {
                        reject('Missing attachments property')
                    } else if (!('tags' in msg)) {
                        reject('Missing tags property')
                    }
                })
                resolve([messages])
            }
        })
    })
}, onError)

.catch(function(err) {
    console.error('\x1b[31m%s\x1b[0m', err)
    process.exit()
})

.then(function(args) {
    [messages] = args
    console.log('\x1b[32m%s\x1b[0m', 'Test success')
    process.exit()
})


/********* Functions *********/

function onError(err) {
    throw err
}

// recursive loop until db is ready
function getCustomer(username) {
    return new Promise(function (resolve, reject) {
        app.models.Customer.findOne({where: {username: username}}, function (err, customer) {
            if (err || !customer) {
                getCustomer(username).then(resolve, reject)
            } else {
                resolve([customer])
            }
        })
    })
}
