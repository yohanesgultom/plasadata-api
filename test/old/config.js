// test config & constants
var server = process.argv.length > 2 ? process.argv[2] : 'http://localhost:3000'

module.exports = {
    server: server,
    apiUrl: server + '/api',
    headerData: {
        "Content-Type": "application/json",
        "Accept": "application/json"
    },
    rand: function (min, max) {
        return Math.round((Math.random() * max) + min)
    }
}
