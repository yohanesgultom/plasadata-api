// reusable authentication scripts

var request = require('request'),
    config = require('./config'),
    headerData = config.headerData

module.exports = {

    login: function (loginData, callback) {
        var url = config.apiUrl + '/Customers/login',
            accessToken = null,
            userId = null
        console.log('POST ' + url)
        request.post({url: url, headers: headerData, body: JSON.stringify(loginData)}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (err) {
                // do nothing
            } else if (body.error) {
                err = body.error
            } else {
                accessToken = body.id
                userId = body.userId
            }
            if (callback) callback(err, accessToken, userId)
        })
    },

    logout: function (accessToken, callback) {
        var url = config.apiUrl + '/Customers/logout?access_token=' + accessToken
        console.log('POST ' + url)
        request.post({url: url}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (err) {
                // do nothing
            } else if (body.error) {
                err = body.error
            }
            if (callback) callback(err)
        })
    },

    thirdPartyLogin: function (loginData, callback) {
        var url = config.apiUrl + '/Customers/third-party-login',
            accessToken = null,
            userId = null
        console.log('POST ' + url)
        request.post({url: url, headers: headerData, body: JSON.stringify(loginData)}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (err) {
                // do nothing
            } else if (body.error) {
                err = body.error
            } else {
                accessToken = body.id
                userId = body.userId
            }
            if (callback) callback(err, accessToken, userId)
        })
    }
    
}
