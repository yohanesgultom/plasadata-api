// bundles tests

var request = require('request'),
    config = require('./config'),
    auth = require('./auth'),
    prod = require('./product'),
    cust = require('./customer'),
    Cart = require('./cart'),
    headerData = config.headerData,
    time = new Date().getTime(),
    loginData = {
        username: "buyer2",
        password: "buyer123",
    },
    globalAccessToken,
    balance,
    uniques,
    duplicates

/* Main test */

new Promise(function (resolve, reject) {
    auth.login(loginData, function(err, accessToken, userId) {
        if (err) {
            reject(err)
        } else {
            globalAccessToken = accessToken
            resolve([accessToken, userId])
        }
    })
})

.then(function(args) {
    [accessToken, userId] = args
    return new Promise(function (resolve, reject) {
        cust.getProfile(accessToken, userId, function(err, customer) {
            if (err) {
                reject(err)
            } else {
                balance = customer.balance
                resolve([accessToken, userId])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId] = args
    return new Promise(function (resolve, reject) {
        getBundles(function(err, bundles) {
            if (err) {
                reject(err)
            } else {
                var valid = true
                for (var i = 0; i < bundles.length; i++) {
                    if (!validBundlePrice(bundles[i])) {
                        valid = false
                        break
                    }
                }
                if (!valid) {
                    reject('Invalid bundle(s) price')
                } else {
                    resolve([accessToken, userId, bundles])
                }
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, bundles] = args,
    [uniques, duplicates] = separateBundles(bundles)
    var someBundles = uniques.slice(0, rand(1, uniques.length)),
        someBundlesIds = someBundles.map(function(b) {  return b.id }),
        someBundlesProducts = []
    someBundles.forEach(function(b) {
        b.products.forEach(function(p) {
            someBundlesProducts.push(p)
        })
    })
    return new Promise(function (resolve, reject) {
        Cart.updateCart(userId, accessToken, [], someBundlesIds, function(err, cart) {
            if (err) {
                reject(err)
            } else if (cart.bundles.length != someBundles.length) {
                reject('Unexpected cart items count ' + cart.bundles.length)
            } else {
                resolve([accessToken, userId, someBundlesProducts])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, someBundlesProducts] = args
    return new Promise(function (resolve, reject) {
        Cart.checkout(userId, accessToken, function(err, purchases) {
            if (err) {
                reject(err)
            } else if (purchases.length != someBundlesProducts.length) {
                reject('Unexpected purchases count ' + purchases.length)
            } else {
                resolve([accessToken, userId, purchases])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, purchases] = args
    return new Promise(function (resolve, reject) {
        Cart.getCart(userId, accessToken, function(err, cart) {
            if (err) {
                reject(err)
            } else if (cart.bundles.length > 0) {
                reject('Cart is not empty')
            } else {
                resolve([accessToken, userId, purchases])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, purchases] = args
    return new Promise(function (resolve, reject) {
        cust.getProfile(accessToken, userId, function(err, customer) {
            if (err) {
                reject(err)
            } else if (customer.balance >= balance) {
                reject('Unsuccessful checkout')
            } else {
                resolve([accessToken, userId, purchases])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, purchases] = args
    var purchaseIds = purchases.map(function(p) { return p.id })
    return new Promise(function (resolve, reject) {
        cust.refund(accessToken, userId, purchaseIds, function(err, refunds) {
            if (err) {
                reject(err)
            } else if (purchases.length != refunds.length) {
                reject('Unexpected refunds count ' + refunds.length)
            } else {
                resolve([accessToken, userId, refunds])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId] = args
    return new Promise(function (resolve, reject) {
        cust.getProfile(accessToken, userId, function(err, customer) {
            if (err) {
                reject(err)
            } else if (Math.abs(balance - customer.balance) >= 0.1) {
                reject('Unsuccessful refunds ' + balance + ' expected, but ' + customer.balance + ' found' )
            } else {
                resolve([accessToken, userId])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId] = args
    var someBundles = duplicates.slice(0, rand(2, duplicates.length)),
        someBundlesIds = someBundles.map(function(b) {  return b.id })
    return new Promise(function (resolve, reject) {
        Cart.validateCart(userId, accessToken, [], someBundlesIds, function(err, conflicts) {
            if (err) {
                reject(err)
            } else {
                resolve([accessToken, userId])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId] = args
    var someBundles = uniques.slice(0, rand(2, uniques.length)),
        someBundlesIds = someBundles.map(function(b) {  return b.id })
    return new Promise(function (resolve, reject) {
        Cart.validateCart(userId, accessToken, [], someBundlesIds, function(err, conflicts) {
            if (err) {
                reject(err)
            } else if (Object.keys(conflicts).length > 0) {
                reject('Detecting conflicts on valid request')
            } else {
                resolve([accessToken, userId])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId] = args
    var someBundles = duplicates.slice(0, rand(2, duplicates.length)),
        someBundlesIds = someBundles.map(function(b) {  return b.id })
    return new Promise(function (resolve, reject) {
        Cart.updateCart(userId, accessToken, [], someBundlesIds, function(err, cart) {
            if (err) {
                resolve([accessToken, userId])
            } else {
                reject('Duplicates accepted')
            }
        })
    })
})

.catch(function(err) {
    console.log(err)
    console.error('\x1b[31m%s\x1b[0m', err)
    auth.logout(globalAccessToken, function(err) {
        if (err) throw err
        console.warn('\x1b[31m%s\x1b[0m', 'Test failed')
    })
})

.then(function(args) {
    [accessToken, userId] = args
    auth.logout(globalAccessToken, function(err) {
        if (err) throw err
        console.log('\x1b[32m%s\x1b[0m', 'Test success')
    })
})

/** Functions **/

function rand(min, max) {
    return Math.round((Math.random() * max) + min)
}

function getBundles(callback, filter) {
    var url = config.apiUrl + '/bundles'
    if (filter) {
        url += '?filter=' + JSON.stringify(filter)
    }
    console.log('GET ' + url)
    request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
        body = body ? JSON.parse(body) : body
        if (callback) callback(err, body)
    })
}

function validBundlePrice(bundle) {
    var productsPrice = 0.0
    bundle.products.forEach(function(p) {
        productsPrice += p.basePrice
    })
    productsPrice = Math.round((productsPrice - (productsPrice * bundle.discount / 100)) * 100) / 100
    if (productsPrice != bundle.price) {
        console.log(bundle)
        console.log(productsPrice + ' vs ' + bundle.price)
    }
    return productsPrice == bundle.price
}

function separateBundles(bundles) {
    var productIds = {},
        uniques = [],
        duplicates = {}
    bundles.forEach(function(b) {
        var unique = true,
            conflictBundle
        b.products.forEach(function(p) {
            if (productIds[p.id]) {
                conflictBundle = productIds[p.id]
                unique = false
            } else {
                productIds[p.id] = b
            }
        })
        if (unique) {
            uniques.push(b)
        } else {
            duplicates[b.id] = b
            duplicates[conflictBundle.id] = conflictBundle
        }
    })
    duplicates = values(duplicates)
    return [uniques, duplicates]
}

function values(obj) {
    var values = []
    Object.keys(obj).forEach(function(key) {
        values.push(obj[key])
    })
    return values
}
