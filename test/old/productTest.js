// product test

// GET http://localhost:3000/api/Products
// GET http://localhost:3000/api/Products/count?where={"tags":{"inq":["music"]}}
// GET http://localhost:3000/api/Products?filter={"where":{"tags":{"inq":["music"]}},"limit":2,"offset":1}
// GET http://localhost:3000/api/Products/tags
// GET http://localhost:3000/api/Products/tags/music
// GET http://localhost:3000/api/Products/tags?limit=3

var prod = require('./product'),
    count = 0

/* Main program */

prod.getProducts(function(err, products) {
    if (err) throw err
    if (products.length <= 0) throw 'No product returned'
    prod.countProducts(function(err, res) {
        if (err) throw err
        if (!res.count || res.count <= 0) throw 'Count returns unexpected value: ' + res.count
        count = res.count
        prod.getProducts(function(err, musics) {
            if (err) throw err
            if (musics.length != 2) throw 'Unexpected returns length: ' + musics.length
            prod.getTags(function(err, tags) {
                if (err) throw err
                if (!tags || tags.length < 0) throw 'Empty tags'
                prod.getTags(function(err, tags) {
                    if (err) throw err
                    if (!tags || tags.length > 3) throw 'Number of tags exceed limit'
                    prod.getSubTags('music', function(err, subtags) {
                        if (err) throw err
                        if (!subtags || subtags.length < 0) throw 'Empty subtags'
                        prod.getTopSellingProducts(function (err, products) {
                            if (err) throw err
                            if (!products || products.length <= 0) throw 'no product found'
                            if (products.length != 1) throw 'result exceeds limit'
                            var expectedLength = 2
                            prod.getPromotedProducts(function (err, promoteds) {
                                if (err) throw err
                                if (!promoteds || promoteds.length <= 0) throw 'no promoted product found'
                                if (promoteds.length != expectedLength) throw 'result exceeds limit: ' + promoteds.length
                                var counts = []
                                prod.countPromotedProducts(function (err, res) {
                                    if (err) throw err
                                    counts.push(res.count)
                                    prod.countPromotedProducts(function (err, res) {
                                        if (err) throw err
                                        counts.push(res.count)
                                        prod.countPromotedProducts(function (err, res) {
                                            if (err) throw err
                                            counts.push(res.count)
                                            prod.countPromotedProducts(function (err, res) {
                                                if (err) throw err
                                                var expectedTotal = res.count,
                                                    actualTotal = counts.reduce(function (a, b) { return a + b}, 0)
                                                if (expectedTotal != actualTotal) throw 'unexpected total count: ' + actualTotal + '. Expected: ' + expectedTotal
                                                console.log('\x1b[32m%s\x1b[0m', 'Test success')
                                            })
                                        }, 'ebook')
                                    }, 'music')
                                }, 'video')
                            }, 'video', expectedLength, 1)
                        }, 1, 1)
                    })
                }, 3)
            })
        }, {where: {tags: {inq: ['music']}}, limit: 2, offset: 1})
    }, ['music'])
})
