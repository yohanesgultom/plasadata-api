// purchasetest
// TODO not providing immutabilty

var request = require('request'),
    config = require('./config'),
    auth = require('./auth'),
    prod = require('./product'),
    cust = require('./customer'),
    headerData = config.headerData,
    time = time = new Date().getTime(),
    // loginData = {
    //     firstName: time.toString(),
    //     lastName: "buyer",
    //     gender: "f",
    //     email: time.toString() + "@yahoo.co.id",
    //     username: "buyer" + time.toString(),
    //     password: "buyer123"
    // },
    loginData = {
        username: 'buyer3',
        password: 'buyer123'
    },
    balance = 0

/* Main test */

// TODO provides immutabilty
console.warn('\x1b[93m%s %s\x1b[0m', process.argv[1], 'changes databases')

// cust.register(loginData.firstName, loginData.lastName, loginData.gender, loginData.email, loginData.username, loginData. password, function (err, customer) {
    auth.login(loginData, function(err, accessToken, userId) {
        if (err) throw err
        cust.getProfile(accessToken, userId, function(err, customer) {
            if (err) throw err
            balance = customer.balance
            prod.getProducts(function(err, products) {
                if (err) throw err
                cust.purchase(accessToken, userId, products[0].id, function(err, purchases) {
                    if (err) throw err
                    if (purchases.length != 1) throw 'invalid purchased count: ' + purchases.length
                    cust.getProfile(accessToken, userId, function(err, customer) {
                        if (err) throw err
                        if (balance == customer.balance) throw 'Incorrect balance'
                        cust.downloadProduct(accessToken, userId, products[0].id, function(err, download) {
                            if (err) throw err
                            cust.refund(accessToken, userId, [purchases[0].id], function(err, purchases) {
                                if (err) throw err
                                if (purchases.length != 1) throw 'invalid purchased count: ' + purchases.length
                                cust.getProfile(accessToken, userId, function(err, customer) {
                                    if (err) throw err
                                    if (balance != customer.balance) throw 'Incorrect balance'
                                    cust.downloadProduct(accessToken, userId, products[0].id, function(err, download) {
                                        if (!err) throw 'Unauthorized download allowed'
                                        auth.logout(accessToken, function(err) {
                                            if (err) throw err
                                            console.log('\x1b[32m%s\x1b[0m', 'Test success')
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            // avoid getting subscription
            }, {limit: 1, where: {basePrice: {lt: 20000}, tags: {inq: ['music','video','ebook']}}})
        })
    })
// })
