// wishlists tests

var request = require('request'),
    config = require('./config'),
    auth = require('./auth'),
    prod = require('./product'),
    cust = require('./customer'),
    headerData = config.headerData,
    time = new Date().getTime(),
    existingLoginData = {
        username: "buyer",
        password: "buyer123",
    },
    globalAccessToken

/* Main test */

new Promise(function (resolve, reject) {
    auth.login(existingLoginData, function(err, accessToken, userId) {
        if (err) {
            reject(err)
        } else {
            globalAccessToken = accessToken
            resolve([accessToken, userId])
        }
    })
})

.then(function(args) {
    [accessToken, userId] = args
    return new Promise(function (resolve, reject) {
        getMessages(userId, accessToken, function(err, messages) {
            if (err) {
                reject(err)
            } else {
                resolve([accessToken, userId, messages])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, messages] = args
    var messageIds = messages.slice(0, Math.round(messages.length / 2)).map(function(m) {return m.id})
    return new Promise(function (resolve, reject) {
        markMessages(messageIds, 1, userId, accessToken, function(err) {
            if (err) {
                reject(err)
            } else {
                resolve([accessToken, userId, messageIds])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, messageIds] = args
    return new Promise(function (resolve, reject) {
        getMessages(userId, accessToken, function(err, messages) {
            if (err) {
                reject(err)
            } else if (messages.length != messageIds.length) {
                reject('Unexpected read messages: ' + messages.length)
            } else {
                resolve([accessToken, userId, messageIds])
            }
        }, {where: {status: 1}})
    })
})

.then(function(args) {
    [accessToken, userId, messageIds] = args
    return new Promise(function (resolve, reject) {
        markMessages(messageIds, 0, userId, accessToken, function(err) {
            if (err) {
                reject(err)
            } else {
                resolve([accessToken, userId, messageIds])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, messageIds] = args
    return new Promise(function (resolve, reject) {
        getMessages(userId, accessToken, function(err, messages) {
            if (err) {
                reject(err)
            } else if (messages.length != 0) {
                reject('Unexpected read messages: ' + messages.length)
            } else {
                resolve([accessToken, userId])
            }
        }, {where: {status: 1}})
    })
})

.catch(function(err) {
    console.error('\x1b[31m%s\x1b[0m', err)
    auth.logout(globalAccessToken, function(err) {
        if (err) throw err
        console.warn('\x1b[31m%s\x1b[0m', 'Test failed')
    })
})

.then(function(args) {
    [accessToken, userId] = args
    auth.logout(accessToken, function(err) {
        if (err) throw err
        console.log('\x1b[32m%s\x1b[0m', 'Test success')
    })

})

/** Functions **/

function getMessages(customerId, accessToken, callback, filter) {
    var url = config.apiUrl + '/Customers/' + customerId + '/messages?access_token=' + accessToken

    if (filter) {
        url += '&filter=' + JSON.stringify(filter)
    }

    console.log('GET ' + url)
    request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
        body = body ? JSON.parse(body) : body
        if (callback) callback(err, body)
    })
}

function markMessages(messageIds, status, customerId, accessToken, callback) {
    var url = config.apiUrl + '/Customers/' + customerId + '/messages?access_token=' + accessToken,
        data = {
            messageIds: messageIds,
            status: status
        }

    console.log('PUT ' + url)
    request.put({url: url, headers: headerData, body: JSON.stringify(data)}, function (err, httpResponse, body) {
        body = body ? JSON.parse(body) : body
        if (callback) callback(err, body)
    })
}
