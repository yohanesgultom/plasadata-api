// customer tests

var fs = require('fs'),
    request = require('request'),
    config = require('./config'),
    auth = require('./auth'),
    prod = require('./product'),
    cust = require('./customer'),
    time = new Date().getTime(),
    loginData = {
        firstName: time.toString(),
        lastName: "buyer",
        gender: "f",
        email: time.toString() + "@yahoo.co.id",
        username: "buyer" + time.toString(),
        password: "buyer123"
    },
    existingLoginData = {
        username: "buyer",
        password: "buyer123",
    }

/* Main test */

// TODO provides immutabilty
console.warn('\x1b[93m%s %s\x1b[0m', process.argv[1], 'changes databases')

cust.register(loginData.firstName, loginData.lastName, loginData.gender, loginData.email, loginData.username, loginData. password, function (err, body) {
    if (err) throw err
    auth.login(existingLoginData, function(err, accessToken, userId) {
        if (err) throw err
        var photoData = {
          value:  fs.createReadStream(__dirname + '/anonymous.jpg'),
          options: {
            contentType: 'image/jpg'
          }
        }
        cust.getProfile(accessToken, userId, function(err, customer) {
            if (err) throw err
            auth.logout(accessToken, function(err) {
                if (err) throw err
                auth.login(existingLoginData, function(err, accessToken, userId) {
                    if (err) throw err
                    var photoData = {
                      value:  fs.createReadStream(__dirname + '/anonymous.jpg'),
                      options: {
                        contentType: 'image/jpg'
                      }
                    }
                    cust.uploadPhoto(accessToken, userId, photoData, function(err) {
                        if (err) throw err
                        auth.logout(accessToken, function(err) {
                            if (err) throw err
                            console.log('\x1b[32m%s\x1b[0m', 'Test success')
                        })
                    })
                })
            })

        })
    })
})
