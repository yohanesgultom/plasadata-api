var request = require('request'),
    config = require('./config'),
    auth = require('./auth'),
    cust = require('./customer'),
    headerData = config.headerData,
    loginData = {username: 'buyer', password: 'buyer123'},
    globalAccessToken = null,
    initialBalance = null,
    topupAmount = null

// TODO provides immutabilty
console.warn('\x1b[93m%s %s\x1b[0m', process.argv[1], 'changes databases')

new Promise(function (resolve, reject) {
    auth.login(loginData, function(err, accessToken, userId) {
        if (err) {
            reject(err)
        } else {
            globalAccessToken = accessToken
            resolve([accessToken, userId])
        }
    })
})

.then(function(args) {
    [accessToken, userId] = args
    return new Promise(function (resolve, reject) {
        cust.getProfile(accessToken, userId, function(err, customer) {
            if (err) {
                reject(err)
            } else if (!customer || !customer.balance) {
                reject('Invalid customer and/or balance')
            } else {
                initialBalance = customer.balance
                resolve([accessToken, userId])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId] = args
    return new Promise(function (resolve, reject) {
        topupAmount = 500000
        getTopupConfig(topupAmount, userId, accessToken, function(err, topupConfig) {
            if (err) {
                reject(err)
            } else {
                if (!topupConfig.veritransSnapToken
                    || !topupConfig.veritransClientKey
                    || !topupConfig.orderId
                    || !topupConfig.amount) {
                    reject(err)
                } else {
                    resolve([topupConfig, accessToken, userId])
                }
            }
        })
    })
})

.then(function(args) {
    [topupConfig, accessToken, userId] = args
    return new Promise(function (resolve, reject) {
        finishPayment(topupConfig.orderId, accessToken, function(err, payment) {
            if (err) {
                reject(err)
            } else {
                if (payment.status != 'Finished') {
                    reject('Incorrect status: ' + payment.status + '. Expected: Finished')
                } else if (payment.orderId != topupConfig.orderId) {
                    reject('Incorrect orderId: ' + payment.orderId + '. Expected: ' + topupConfig.orderId)
                } else {
                    resolve([topupConfig, accessToken, userId])
                }
            }
        })
    })
})

.then(function(args) {
    [topupConfig, accessToken, userId] = args
    return new Promise(function (resolve, reject) {
        unfinishPayment(topupConfig.orderId, accessToken, function(err, payment) {
            if (err) {
                reject(err)
            } else {
                if (payment.status != 'Unfinished') {
                    reject('Incorrect status: ' + payment.status + '. Expected: Unfinished')
                } else if (payment.orderId != topupConfig.orderId) {
                    reject('Incorrect orderId: ' + payment.orderId + '. Expected: ' + topupConfig.orderId)
                } else {
                    resolve([topupConfig, accessToken, userId])
                }
            }
        })
    })
})

.then(function(args) {
    [topupConfig, accessToken, userId] = args
    return new Promise(function (resolve, reject) {
        errorPayment(topupConfig.orderId, accessToken, function(err, payment) {
            if (err) {
                reject(err)
            } else {
                if (payment.status != 'Error') {
                    reject('Incorrect status: ' + payment.status + '. Expected: Error')
                } else if (payment.orderId != topupConfig.orderId) {
                    reject('Incorrect orderId: ' + payment.orderId + '. Expected: ' + topupConfig.orderId)
                } else {
                    resolve([topupConfig, accessToken, userId])
                }
            }
        })
    })
})

.then(function(args) {
    [topupConfig, accessToken, userId] = args
    return new Promise(function (resolve, reject) {
        var payload = {
          status_code : "200",
          status_message : "Success, transaction found",
          transaction_id : "249fc620-6017-4540-af7c-5a1c25788f46",
          masked_card : "481111-1114",
          order_id : 'example-1424936368',
          payment_type : "credit_card",
          transaction_time : "2015-02-26 14:39:33",
          transaction_status : "capture",
          fraud_status : "accept",
          approval_code : "1424936374393",
          signature_key : "e12238a7bf89a71f2aaf2f32829f7245fe04cd57513ce1a3bae0c1f7bdf9f48e2cd3e3b49f36ad5008d1d602d8b5a0dd9091af3c5d08dad9608a1ac6f5ae344b",
          bank : "bni",
          gross_amount : "30000.00"
        }
        notify(payload, function(err, payment) {
            if (err) {
                if (err.message.startsWith('No corresponding orderId')) {
                    resolve([accessToken, userId])
                } else {
                    reject(err)
                }
            } else {
                if (payment.status != 'Notified') {
                    reject('Incorrect status: ' + payment.status + '. Expected: Notified')
                } else if (!payment.payload) {
                    reject('Empty payload')
                } else {
                    resolve([accessToken, userId])
                }
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId] = args
    return new Promise(function (resolve, reject) {
        var orderId = 'Sample Order-1'
        getTopupStatus(orderId, userId, accessToken, function(err, payment) {
            if (err) {
                if (err.message.startsWith('No corresponding orderId')) {
                    resolve([accessToken, userId])
                } else {
                    reject(err)
                }
            } else {
                if (payment.status != 'Confirmed') {
                    reject('Incorrect status: ' + payment.status + '. Expected: Confirmed')
                } else if (!payment.payload) {
                    reject('Empty payload')
                } else {
                    resolve([accessToken, userId])
                }
            }
        })
    })
})

.catch(function(err) {
    console.log(err)
    console.error('\x1b[31m%s\x1b[0m', err)
    auth.logout(globalAccessToken, function(err) {
        if (err) throw err
        console.warn('\x1b[31m%s\x1b[0m', 'Test failed')
    })
})

.then(function(args) {
    [accessToken, userId] = args
    auth.logout(globalAccessToken, function(err) {
        if (err) throw err
        console.log('\x1b[32m%s\x1b[0m', 'Test success')
    })
})

/************* functions ****************/

function getTopupConfig(amount, userId, accessToken, cb) {
    var url = config.apiUrl + '/Customers/' + userId + '/topup?access_token=' + accessToken,
        data = {
            amount: amount
        }
    console.log('POST ' + url)
    request.post({url: url, headers: headerData, body: JSON.stringify(data)}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (err) {
                // do nothing
            } else if (body.error) {
                err = body.error
            }
            cb(err, body)
        }
    )
}

function getTopupStatus(orderId, userId, accessToken, cb) {
    var url = config.apiUrl + '/Customers/' + userId + '/topup-status/' + orderId + '?access_token=' + accessToken
    console.log('GET ' + url)
    request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (err) {
                // do nothing
            } else if (body.error) {
                err = body.error
            }
            cb(err, body)
        }
    )
}

function finishPayment(orderId, accessToken, cb) {
    var url = config.apiUrl + '/Payments/finish'
    url += '?order_id=' + orderId
    console.log('GET ' + url)
    request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (err) {
                // do nothing
            } else if (body.error) {
                err = body.error
            }
            cb(err, body)
        }
    )
}

function unfinishPayment(orderId, accessToken, cb) {
    var url = config.apiUrl + '/Payments/unfinish'
    url += '?order_id=' + orderId
    console.log('GET ' + url)
    request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (err) {
                // do nothing
            } else if (body.error) {
                err = body.error
            }
            cb(err, body)
        }
    )
}

function errorPayment(orderId, accessToken, cb) {
    var url = config.apiUrl + '/Payments/error'
    url += '?order_id=' + orderId
    console.log('GET ' + url)
    request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (err) {
                // do nothing
            } else if (body.error) {
                err = body.error
            }
            cb(err, body)
        }
    )
}

function notify(payload, cb) {
    var url = config.apiUrl + '/Payments/notification'
    console.log('POST ' + url)
    request.post({url: url, headers: headerData, body: JSON.stringify(payload)}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (err) {
                // do nothing
            } else if (body.error) {
                err = body.error
            }
            cb(err, body)
        }
    )
}
