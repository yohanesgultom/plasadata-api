
var fs = require('fs'),
    request = require('request'),
    config = require('./config'),
    headerData = config.headerData,
    tag = 'ebook'

getPromotions(function(err, promotions) {
    if (err) throw err
    if (!promotions || promotions.length <= 0) throw 'No promotions returned'
    getPromotions(function(err, promotions) {
        if (err) throw err
        if (!promotions || promotions.length <= 0) throw 'No promotions returned'
        getPromotedProducts(promotions[0].id, function(err, products) {
            if (err) throw err
            if (!products || products.length <= 0) throw 'No products returned'
            getPromotions(function(err, promotions) {
                if (err) throw err
                if (!promotions || promotions.length <= 0) throw 'No promotions returned'
                promotions.forEach(function(promo) {
                    if (promo.products && promo.products.length > 0) {
                        promo.products.forEach(function(prod) {
                            var found = false
                            for (var i = 0; i < prod.tags.length; i++) {
                                if (prod.tags[i] == tag) {
                                    found = true
                                    break
                                }
                            }
                            if (!found) throw 'Promotion\'s products are not filtered properly: ' + JSON.stringify(prod)
                        })
                    }
                })
                console.log('\x1b[32m%s\x1b[0m', 'Test success')
            }, {include: {relation: 'products', scope:{ where: {tags:{inq:[tag]}}}}})
        })
    }, {include: ['products']})
})


function getPromotions(callback, filter) {
    var url = config.apiUrl + '/Promotions'
    if (filter) {
        url += '?filter=' + JSON.stringify(filter)
    }
    console.log('GET ' + url)
    request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
        body = body ? JSON.parse(body) : body
        if (callback) callback(err, body)
    })
}

function getPromotedProducts(id, callback, filter) {
    var url = config.apiUrl + '/Promotions/' + id + '/products'
    if (filter) {
        url += '?filter=' + JSON.stringify(filter)
    }
    console.log('GET ' + url)
    request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
        body = body ? JSON.parse(body) : body
        if (callback) callback(err, body)
    })
}
