var request = require('request'),
    config = require('./config'),
    headerData = config.headerData

module.exports = {

    getCart: function (userId, accessToken, callback) {
        var url = config.apiUrl + '/Customers/' + userId + '/cart?access_token=' + accessToken
        console.log('GET ' + url)
        request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            err = !err && body.error ? body.error : err
            err = (err && typeof err != 'string') ? err.message : err
            if (callback) callback(err, body)
        })
    },

    validateCart: function (userId, accessToken, products, bundles, callback) {
        var url = config.apiUrl + '/Customers/' + userId + '/cart/validate?access_token=' + accessToken,
            data = {
                products: products,
                bundles: bundles
            }
        request.post({url: url, headers: headerData, body: JSON.stringify(data)}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            err = !err && body.error ? body.error : err
            err = (err && typeof err != 'string') ? err.message : err
            if (callback) callback(err, body)
        })
    },

    updateCart: function (userId, accessToken, products, bundles, callback) {
        var url = config.apiUrl + '/Customers/' + userId + '/cart?access_token=' + accessToken,
            data = {
                products: products,
                bundles: bundles
            }
        console.log('PUT ' + url)
        request.put({url: url, headers: headerData, body: JSON.stringify(data)}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            err = !err && body.error ? body.error : err
            err = (err && typeof err != 'string') ? err.message : err
            if (callback) callback(err, body)
        })
    },

    checkout: function (userId, accessToken, callback) {
        var url = config.apiUrl + '/Customers/' + userId + '/checkout?access_token=' + accessToken
        console.log('POST ' + url)
        request.post({url: url, headers: headerData}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            err = !err && body.error ? body.error : err
            err = (err && typeof err != 'string') ? err.message : err
            if (callback) callback(err, body)
        })
    },

    checkoutAsGift: function (userId, recipientUsername, accessToken, callback) {
        var url = config.apiUrl + '/Customers/' + userId + '/checkout-gift/' + recipientUsername + '?access_token=' + accessToken
        console.log('POST ' + url)
        request.post({url: url, headers: headerData}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            err = !err && body.error ? body.error : err
            err = (err && typeof err != 'string') ? err.message : err
            if (callback) callback(err, body)
        })
    },

}
