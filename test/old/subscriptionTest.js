// subscription tests

var request = require('request'),
    config = require('./config'),
    auth = require('./auth'),
    prod = require('./product'),
    cust = require('./customer'),
    headerData = config.headerData,
    time = new Date().getTime(),
    existingLoginData = {
        username: "buyer2",
        password: "buyer123",
    },
    globalAccessToken

/* Main test */

new Promise(function (resolve, reject) {
    auth.login(existingLoginData, function(err, accessToken, userId) {
        if (err) {
            reject(err)
        } else {
            globalAccessToken = accessToken
            resolve([accessToken, userId])
        }
    })
})

.then(function(args) {
    [accessToken, userId] = args
    return new Promise(function (resolve, reject) {
        prod.getProducts(function(err, subscriptions) {
            if (err) {
                reject(err)
            } else if (!subscriptions || subscriptions.length < 0) {
                reject('Empty subscriptions')
            } else {
                resolve([accessToken, userId, subscriptions])
            }
        }, {where: {tags: {inq: ['subscription']}}})
    })
})

.then(function(args) {
    [accessToken, userId, subscriptions] = args,
    randIndex = config.rand(0, subscriptions.length-1),
    randSubscription = subscriptions[randIndex]

    return new Promise(function (resolve, reject) {
        prod.getSubscribedItems(randSubscription.id, function(err, items) {
            if (err) {
                reject(err)
            } else if (!items || items.length <= 0) {
                reject('empty subscription item')
            } else {
                resolve([accessToken, userId, randSubscription, items])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, randSubscription, items] = args,
    randIndex = config.rand(0, items.length-1),
    randItem = items[randIndex]
    return new Promise(function (resolve, reject) {
        cust.downloadSubscriptionItem(accessToken, userId, randItem.id, function(err, item) {
            if (err) {
                resolve([accessToken, userId, randSubscription, items, randItem])
            } else {
                reject('Able to download unsubscribed item')
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, randSubscription, items, item] = args
    return new Promise(function (resolve, reject) {
        cust.purchase(accessToken, userId, randSubscription.id, function(err, purchases) {
            if (err) {
                reject(err)
            } else if (!purchases || purchases.length < 0) {
                reject('Empty purchase')
            } else {
                resolve([accessToken, userId, randSubscription, items, item, purchases[0]])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, randSubscription, items, item, purchase] = args
    return new Promise(function (resolve, reject) {
        cust.downloadSubscriptionItem(accessToken, userId, item.id, function(err, item) {
            if (err) {
                reject(err)
            } else {
                resolve([accessToken, userId, randSubscription, items, item, purchase])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, randSubscription, items, item, purchase] = args
    return new Promise(function (resolve, reject) {
        cust.refund(accessToken, userId, [purchase.id], function(err, refund) {
            if (err) {
                reject(err)
            } else {
                resolve([accessToken, userId, randSubscription, items, item])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, randSubscription, items, item] = args
    return new Promise(function (resolve, reject) {
        cust.downloadSubscriptionItem(accessToken, userId, item.id, function(err, item) {
            if (err) {
                resolve([accessToken, userId, randSubscription, items, randItem])
            } else {
                reject('Able to download unsubscribed item')
            }
        })
    })
})

.catch(function(err) {
    console.error('\x1b[31m%s\x1b[0m', err)
    auth.logout(globalAccessToken, function(err) {
        if (err) throw err
        console.warn('\x1b[31m%s\x1b[0m', 'Test failed')
    })
})

.then(function(args) {
    [accessToken, userId, products] = args
    auth.logout(accessToken, function(err) {
        if (err) throw err
        console.log('\x1b[32m%s\x1b[0m', 'Test success')
    })

})
