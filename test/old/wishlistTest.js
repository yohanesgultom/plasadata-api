// wishlists tests

var request = require('request'),
    config = require('./config'),
    auth = require('./auth'),
    prod = require('./product'),
    cust = require('./customer'),
    headerData = config.headerData,
    time = new Date().getTime(),
    existingLoginData = {
        username: "buyer",
        password: "buyer123",
    },
    globalAccessToken

/* Main test */

new Promise(function (resolve, reject) {
    auth.login(existingLoginData, function(err, accessToken, userId) {
        if (err) {
            reject(err)
        } else {
            globalAccessToken = accessToken
            resolve([accessToken, userId])
        }
    })
})

.then(function(args) {
    [accessToken, userId] = args
    return new Promise(function (resolve, reject) {
        getWishlist(userId, accessToken, function(err, wishlists) {
            if (err) {
                reject(err)
            } else {
                // console.log(wishlists)
                resolve([accessToken, userId, wishlists])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId] = args
    return new Promise(function (resolve, reject) {
        prod.getProducts(function(err, products) {
            if (err) {
                reject(err)
            } else if (!products || products.length <= 0) {
                reject('empty products')
            } else {
                resolve([accessToken, userId, products])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, products] = args
    return new Promise(function (resolve, reject) {
        addWishlist(userId, products[0].id, accessToken, function(err, wishlist) {
            if (err) {
                reject(err)
            } else if (!wishlist) {
                reject('invalid wishlist')
            } else {
                resolve([accessToken, userId, products])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, products] = args
    return new Promise(function (resolve, reject) {
        addWishlist(userId, products[1].id, accessToken, function(err, wishlist) {
            if (err) {
                reject(err)
            } else if (!wishlist) {
                reject('invalid wishlist')
            } else {
                resolve([accessToken, userId, products])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, products] = args
    return new Promise(function (resolve, reject) {
        getWishlist(userId, accessToken, function(err, wishlists) {
            if (err) {
                reject(err)
            } else if (wishlists.length != 2) {
                reject('Invalid wishlists count: ' + wishlists.length)
            } else {
                // console.log(wishlists)
                resolve([accessToken, userId, products])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, products] = args
    return new Promise(function (resolve, reject) {
        removeWishlist(userId, products[1].id, accessToken, function(err, wishlists) {
            if (err) {
                reject(err)
            } else {
                resolve([accessToken, userId, products])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, products] = args
    return new Promise(function (resolve, reject) {
        getWishlist(userId, accessToken, function(err, wishlists) {
            if (err) {
                reject(err)
            } else if (wishlists.length != 1) {
                reject('Invalid wishlists count: ' + wishlists.length)
            } else {
                resolve([accessToken, userId, products])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, products] = args
    return new Promise(function (resolve, reject) {
        addWishlist(userId, products[0].id, accessToken, function(err, wishlist) {
            if (err) {
                reject(err)
            } else {
                resolve([accessToken, userId, products])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, products] = args
    return new Promise(function (resolve, reject) {
        addWishlist(userId, products[0].id, accessToken, function(err, wishlist) {
            if (err) {
                reject(err)
            } else {
                resolve([accessToken, userId, products])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, products] = args
    return new Promise(function (resolve, reject) {
        getWishlist(userId, accessToken, function(err, wishlists) {
            if (err) {
                reject(err)
            } else if (wishlists.length > 1) {
                reject('Allowing duplicate')
            } else {
                resolve([accessToken, userId, products])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, products] = args
    auth.logout(accessToken, function(err) {
        if (err) throw err
        console.log('\x1b[32m%s\x1b[0m', 'Test success')
    })

})

.catch(function(err) {
    console.error('\x1b[31m%s\x1b[0m', err)
    auth.logout(globalAccessToken, function(err) {
        if (err) throw err
        console.warn('\x1b[31m%s\x1b[0m', 'Test failed')
    })
})

/** Functions **/

function addWishlist(customerId, productId, accessToken, callback, filter) {
    var url = config.apiUrl + '/Customers/' + customerId + '/wishlists/rel/' + productId + '?access_token=' + accessToken

    console.log('PUT ' + url)
    request.put({url: url, headers: headerData}, function (err, httpResponse, body) {
        body = body ? JSON.parse(body) : body
        if (callback) callback(err, body)
    })
}

function removeWishlist(customerId, productId, accessToken, callback, filter) {
    var url = config.apiUrl + '/Customers/' + customerId + '/wishlists/rel/' + productId + '?access_token=' + accessToken

    console.log('DELETE ' + url)
    request.delete({url: url, headers: headerData}, function (err, httpResponse, body) {
        body = body ? JSON.parse(body) : body
        if (callback) callback(err, body)
    })
}

function getWishlist(customerId, accessToken, callback, filter) {
    var url = config.apiUrl + '/Customers/' + customerId + '/wishlists?access_token=' + accessToken
    if (filter) {
        url += '?filter=' + JSON.stringify(filter)
    }
    console.log('GET ' + url)
    request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
        body = body ? JSON.parse(body) : body
        if (callback) callback(err, body)
    })
}
