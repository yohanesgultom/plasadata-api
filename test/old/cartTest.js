// shopping cart test

var request = require('request'),
    config = require('./config'),
    auth = require('./auth'),
    prod = require('./product'),
    cust = require('./customer'),
    Cart = require('./cart'),
    loginData = {
        username: "buyer2",
        password: "buyer123"
    }

/* Main test */

auth.login(loginData, function(err, accessToken, userId) {
    if (err) throw err
    Cart.getCart(userId, accessToken, function(err, cart) {
        if (err) throw err
        if (!cart) throw 'No cart returned'
        prod.getProducts(function(err, products) {
            if (err) throw err
            var productIds = products.map(function(p) { return p.id })
            // set cart with productIds
            Cart.updateCart(userId, accessToken, productIds, [], function(err, cart) {
                if (err) throw err
                if (cart.products.length != productIds.length) throw 'Unexpected cart products count ' + cart.products.length
                // checkout
                Cart.checkout(userId, accessToken, function(err, purchases) {
                    if (err) throw err
                    if (purchases.length != cart.products.length) throw 'Unexpected purchases count ' + cart.products.length
                    var purchaseIds = purchases.map(function(p) {return p.id})
                    Cart.getCart(userId, accessToken, function(err, cart) {
                        if (err) throw err
                        if (!cart) throw 'No cart returned'
                        if (cart.products.length > 0) throw 'Cart is not empty'
                        cust.refund(accessToken, userId, purchaseIds, function(err, purchases) {
                            if (err) throw err
                            auth.logout(accessToken, function(err) {
                                if (err) throw err
                                console.log('\x1b[32m%s\x1b[0m', 'Test success')
                            })
                        })
                    })
                })
            })
        }, {limit: 2})
    })
})
