// product functions

var fs = require('fs'),
    request = require('request'),
    config = require('./config'),
    auth = require('./auth'),
    headerData = config.headerData

module.exports = {
    getProducts: function (callback, filter) {
        var url = config.apiUrl + '/Products'
        if (filter) {
            url += '?filter=' + JSON.stringify(filter)
        }
        console.log('GET ' + url)
        request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (callback) callback(err, body)
        })
    },

    getProduct: function (id, callback, filter) {
        var url = config.apiUrl + '/Products/' + id
        if (filter) {
            url += '?filter=' + JSON.stringify(filter)
        }
        console.log('GET ' + url)
        request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (callback) callback(err, body)
        })
    },

    countProducts: function (callback, tags) {
        var url = config.apiUrl + '/Products/count'
        if (tags && tags.length > 0) {
            url += '?where=' + JSON.stringify({tags: {inq: tags}})
        }
        console.log('GET ' + url)
        request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (callback) callback(err, body)
        })
    },

    getTags: function (callback, limit) {
        var url = config.apiUrl + '/Products/tags'
        if (limit) {
            url += '?limit=' + limit
        }
        console.log('GET ' + url)
        request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (callback) callback(err, body)
        })
    },

    getSubTags: function (tag, callback, limit) {
        var url = config.apiUrl + '/Products/tags/' + tag
        if (limit) {
            url += '?limit=' + limit
        }
        console.log('GET ' + url)
        request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (callback) callback(err, body)
        })
    },

    getTopSellingProducts: function (callback, limit, offset) {
        var url = config.apiUrl + '/Products/top-selling',
            params = []
        if (limit) {
            params.push('limit=' + limit)
        }
        if (offset) {
            params.push('offset=' + offset)
        }
        if (params.length > 0) {
            url += '?' + params.join('&')
        }
        console.log('GET ' + url)
        request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (callback) callback(err, body)
        })
    },

    getPromotedProducts: function (callback, tag, limit, offset) {
        var url = config.apiUrl + '/Products/promoted',
            params = []
        if (tag) {
            params.push('tag=' + tag)
        }
        if (limit) {
            params.push('limit=' + limit)
        }
        if (offset) {
            params.push('offset=' + offset)
        }
        if (params.length > 0) {
            url += '?' + params.join('&')
        }
        console.log('GET ' + url)
        request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (callback) callback(err, body)
        })
    },

    countPromotedProducts: function (callback, tag) {
        var url = config.apiUrl + '/Products/promoted/count',
            params = []
        if (tag) {
            params.push('tag=' + tag)
        }
        if (params.length > 0) {
            url += '?' + params.join('&')
        }
        console.log('GET ' + url)
        request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (callback) callback(err, body)
        })
    },

    getSubscribedItems: function (subscriptionId, callback) {
        var url = config.apiUrl + '/Products/' + subscriptionId + '/subscriptionItems'
        console.log('GET ' + url)
        request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            body = body ? JSON.parse(body) : body
            if (callback) callback(err, body)
        })
    }

}
