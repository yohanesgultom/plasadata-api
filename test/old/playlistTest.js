// playlist test

var request = require('request'),
    config = require('./config'),
    auth = require('./auth'),
    prod = require('./product'),
    cust = require('./customer'),
    headerData = config.headerData,
    loginData = {
        username: "buyer",
        password: "buyer123"
    }

/* Main test */

new Promise(function (resolve, reject) {
    auth.login(loginData, function(err, accessToken, userId) {
        if (err) {
            reject(err)
        } else {
            globalAccessToken = accessToken
            resolve([accessToken, userId])
        }
    })
})

.then(function(args) {
    [accessToken, userId] = args
    return new Promise(function (resolve, reject) {
        createPlaylist(userId, 'New dummy playlist', accessToken, function (err, playlist) {
            if (err) {
                reject(err)
            } else {
                resolve([accessToken, userId, playlist])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, playlist] = args
    return new Promise(function (resolve, reject) {
        getPlaylists(userId, accessToken, function (err, playlists) {
            if (err) {
                reject(err)
            } else if (!playlists || playlists.length <= 0) {
                reject('empty playlists')
            } else {
                resolve([accessToken, userId, playlists[0]])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, playlist] = args
    return new Promise(function (resolve, reject) {
        cust.getPurchasedProducts(accessToken, userId, function (err, purchased) {
            if (err) {
                reject(err)
            } else if (!purchased || purchased.length <= 0) {
                reject('empty purchased items')
            } else {
                var purchasedIds = purchased.map(function(p) {return p.id})
                resolve([accessToken, userId, playlist, purchasedIds])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, playlist, purchasedIds] = args
    return new Promise(function (resolve, reject) {
        updatePlaylist(playlist.id, userId, accessToken, purchasedIds, function (err, items) {
            if (err) {
                reject(err)
            } else if (!items || items.length <= 0) {
                reject('no items added')
            } else if (items.length != purchasedIds.length) {
                reject('invalid playlist items count:' + items.length)
            } else {
                resolve([accessToken, userId, playlist, purchasedIds])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, playlist, purchasedIds] = args
    return new Promise(function (resolve, reject) {
        getPlaylist(playlist.id, userId, accessToken, function (err, playlist) {
            if (err) {
                reject(err)
            } else if (playlist.products.length != purchasedIds.length) {
                reject('invalid playlist items count' + playlist.products.length)
            } else {
                resolve([accessToken, userId, playlist, purchasedIds])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, playlist, purchasedIds] = args
    return new Promise(function (resolve, reject) {
        var productIds = purchasedIds.splice(0, 1)
        updatePlaylist(playlist.id, userId, accessToken, productIds, function (err, items) {
            if (err) {
                reject(err)
            } else if (!items || items.length <= 0) {
                reject('no items added')
            } else if (items.length != productIds.length) {
                console.log(productIds)
                console.log(items)
                reject('invalid playlist items count:' + items.length)
            } else {
                resolve([accessToken, userId, playlist, purchasedIds, productIds])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, playlist, purchasedIds, productIds] = args
    return new Promise(function (resolve, reject) {
        getPlaylist(playlist.id, userId, accessToken, function (err, playlist) {
            if (err) {
                reject(err)
            } else if (playlist.products.length != productIds.length) {
                reject('invalid playlist items count:' + playlist.products.length)
            } else {
                resolve([accessToken, userId, playlist, purchasedIds])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, playlist, purchasedIds] = args
    return new Promise(function (resolve, reject) {
        deletePlaylist(userId, playlist.id, accessToken, function (err) {
            if (err) {
                reject(err)
            } else {
                resolve([accessToken, userId, playlist, purchasedIds])
            }
        })
    })
})

.then(function(args) {
    [accessToken, userId, playlist, purchasedIds] = args
    return new Promise(function (resolve, reject) {
        getPlaylists(userId, accessToken, function (err, playlists) {
            if (err) {
                reject(err)
            } else if (playlists && playlists.length > 0) {
                reject('playlist deletion failed')
            } else {
                resolve([accessToken, userId])
            }
        })
    })
})

.catch(function(err) {
    console.error('\x1b[31m%s\x1b[0m', err)
    auth.logout(globalAccessToken, function(err) {
        if (err) throw err
        console.warn('\x1b[31m%s\x1b[0m', 'Test failed')
    })
})

.then(function(args) {
    [accessToken, userId] = args
    auth.logout(accessToken, function(err) {
        if (err) throw err
        console.log('\x1b[32m%s\x1b[0m', 'Test success')
    })

})


/* Functions */

function getPlaylists(userId, accessToken, callback) {
    var url = config.apiUrl + '/Customers/' + userId + '/playlists?access_token=' + accessToken
    console.log('GET ' + url)
    request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
        body = body ? JSON.parse(body) : body
        err = !err && body.error ? body.error : err
        if (callback) callback(err, body)
    })
}

function getPlaylist(playlistId, userId, accessToken, callback) {
    var url = config.apiUrl + '/Customers/' + userId + '/playlists/' + playlistId + '?access_token=' + accessToken
    console.log('GET ' + url)
    request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
        body = body ? JSON.parse(body) : body
        err = !err && body.error ? body.error : err
        if (callback) callback(err, body)
    })
}


function createPlaylist(userId, playlistName, accessToken, callback) {
    var url = config.apiUrl + '/Customers/' + userId + '/playlists?access_token=' + accessToken,
        data = {
            name: playlistName
        }
    console.log('POST ' + url)
    request.post({url: url, headers: headerData, body: JSON.stringify(data)}, function (err, httpResponse, body) {
        body = body ? JSON.parse(body) : body
        err = !err && body.error ? body.error : err
        if (callback) callback(err, body)
    })
}

function updatePlaylist(playlistId, userId, accessToken, productIds, callback) {
    var url = config.apiUrl + '/Customers/' + userId + '/playlists/' + playlistId + '/sync?access_token=' + accessToken,
        data = {
            products: productIds
        }
    console.log('PUT ' + url)
    request.put({url: url, headers: headerData, body: JSON.stringify(data)}, function (err, httpResponse, body) {
        body = body ? JSON.parse(body) : body
        err = !err && body.error ? body.error : err
        if (callback) callback(err, body)
    })
}

function deletePlaylist(userId, playlistId, accessToken, callback) {
    var url = config.apiUrl + '/Customers/' + userId + '/playlists/' + playlistId + '?access_token=' + accessToken
    console.log('DELETE ' + url)
    request.delete({url: url, headers: headerData}, function (err, httpResponse, body) {
        body = body ? JSON.parse(body) : body
        err = !err && body.error ? body.error : err
        if (callback) callback(err, body)
    })
}
