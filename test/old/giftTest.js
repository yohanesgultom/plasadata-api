// gift purchase tests

var request = require('request'),
    config = require('./config'),
    auth = require('./auth'),
    prod = require('./product'),
    cust = require('./customer'),
    headerData = config.headerData,
    time = new Date().getTime(),
    existingLoginData = {
        username: "buyer3",
        password: "buyer123",
    },
    recipientLoginData = {
        username: "buyer4",
        password: "buyer123"
    },
    balance = 0,
    globalAccessToken,
    globalUserId,
    productList = [],
    subscriptionList = [],
    randSubscription,
    randSubscriptionItem

// TODO provides immutabilty
console.warn('\x1b[93m%s %s\x1b[0m', process.argv[1], 'changes databases')

new Promise(function (resolve, reject) {
    auth.login(existingLoginData, function(err, accessToken, userId) {
        if (err) {
            reject(err)
        } else {
            globalAccessToken = accessToken
            globalUserId = userId
            resolve([accessToken, userId])
        }
    })
})

.then(function(args) {
    return new Promise(function (resolve, reject) {
        cust.getProfile(globalAccessToken, globalUserId, function(err, profile) {
            if (err) {
                reject(err)
            } else {
                balance = profile.balance
                resolve([profile])
            }
        })
    })
})

.then(function(args) {
    return new Promise(function (resolve, reject) {
        prod.getProducts(function(err, products) {
            if (err) {
                reject(err)
            } else {
                productList = products
                resolve([products])
            }
        })
    })
})

.then(function(args) {
    return new Promise(function (resolve, reject) {
        prod.getProducts(function(err, subscriptions) {
            if (err) {
                reject(err)
            } else if (!subscriptions || subscriptions.length < 0) {
                reject('Empty subscriptions')
            } else {
                subscriptionList = subscriptions
                resolve([subscriptions])
            }
        }, {where: {tags: {inq: ['subscription']}}})
    })
})

.then(function(args) {
    return new Promise(function (resolve, reject) {
        cust.purchaseAsGift(globalAccessToken, globalUserId, recipientLoginData.username, productList[0].id, function(err, purchases) {
            if (err) {
                reject(err)
            } else {
                balance = balance - purchases[0].price
                resolve([purchases])
            }
        })
    })
})

.then(function(args) {
    var randIndex = config.rand(0, subscriptionList.length-1)
    randSubscription = subscriptionList[randIndex]
    return new Promise(function (resolve, reject) {
        cust.purchaseAsGift(globalAccessToken, globalUserId, recipientLoginData.username, randSubscription.id, function(err, purchases) {
            if (err) {
                reject(err)
            } else {
                balance = balance - purchases[0].price
                resolve([purchases])
            }
        })
    })
})

.then(function(args) {
    return new Promise(function (resolve, reject) {
        cust.getProfile(globalAccessToken, globalUserId, function(err, profile) {
            if (err) {
                reject(err)
            } else if (Math.abs(profile.balance - balance) > 0.1) {
                reject('Unexpected remaining balance: ' + profile.balance + '. Expected: ' + balance)
            } else {
                resolve([profile])
            }
        })
    })
})

.then(function(args) {
    return new Promise(function (resolve, reject) {
        auth.logout(globalAccessToken, function(err) {
            if (err) {
                reject(err)
            } else {
                resolve()
            }
        })
    })
})

.then(function(args) {
    return new Promise(function (resolve, reject) {
        auth.login(recipientLoginData, function(err, accessToken, userId) {
            if (err) {
                reject(err)
            } else {
                globalAccessToken = accessToken
                globalUserId = userId
                resolve([accessToken, userId])
            }
        })
    })
})

.then(function(args) {
    return new Promise(function (resolve, reject) {
        cust.downloadProduct(globalAccessToken, globalUserId, productList[0].id, function(err, download) {
            if (err) {
                reject(err)
            } else {
                resolve([download])
            }
        })
    })
})

.then(function(args) {
    return new Promise(function (resolve, reject) {
        prod.getSubscribedItems(randSubscription.id, function(err, items) {
            if (err) {
                reject(err)
            } else if (!items || items.length <= 0) {
                reject('empty subscription item')
            } else {
                var randIndex = config.rand(0, items.length-1)
                randSubscriptionItem = items[randIndex]
                resolve([items])
            }
        })
    })
})

.then(function(args) {
    return new Promise(function (resolve, reject) {
        cust.downloadSubscriptionItem(globalAccessToken, globalUserId, randSubscriptionItem.id, function(err, download) {
            if (err) {
                reject(err)
            } else {
                resolve([download])
            }
        })
    })
})

.then(function(args) {
    auth.logout(globalAccessToken, function(err) {
        if (err) throw err
        console.log('\x1b[32m%s\x1b[0m', 'Test success')
    })
})

.catch(function(err) {
    console.error('\x1b[31m%s\x1b[0m', err)
    auth.logout(globalAccessToken, function(err) {
        if (err) throw err
        console.warn('\x1b[31m%s\x1b[0m', 'Test failed')
    })
})
