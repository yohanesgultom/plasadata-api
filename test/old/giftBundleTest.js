// gift purchase tests

var request = require('request'),
    config = require('./config'),
    auth = require('./auth'),
    prod = require('./product'),
    cust = require('./customer'),
    headerData = config.headerData,
    time = new Date().getTime(),
    existingLoginData = {
        username: "buyer3",
        password: "buyer123",
    },
    recipientLoginData = {
        username: "buyer6",
        password: "buyer123"
    },
    balance = 0,
    globalAccessToken,
    globalUserId,
    randomBundle


// TODO provides immutabilty
console.warn('\x1b[93m%s %s\x1b[0m', process.argv[1], 'changes databases')

new Promise(function (resolve, reject) {
    auth.login(existingLoginData, function(err, accessToken, userId) {
        if (err) {
            reject(err)
        } else {
            globalAccessToken = accessToken
            globalUserId = userId
            resolve([accessToken, userId])
        }
    })
})

.then(function(args) {
    return new Promise(function (resolve, reject) {
        cust.getProfile(globalAccessToken, globalUserId, function(err, profile) {
            if (err) {
                reject(err)
            } else {
                balance = profile.balance
                resolve([profile])
            }
        })
    })
})

.then(function(args) {
    return new Promise(function (resolve, reject) {
        getBundles(function(err, bundles) {
            if (err) {
                reject(err)
            } else {
                var randIndex = config.rand(0, bundles.length-1)
                randomBundle = bundles[randIndex]
                resolve([bundles])
            }
        })
    })
})

.then(function(args) {
    return new Promise(function (resolve, reject) {
        cust.purchaseBundleAsGift(globalAccessToken, globalUserId, recipientLoginData.username, randomBundle.id, function(err, purchases) {
            if (err) {
                reject(err)
            } else {
                purchases.forEach(function (pur) {
                    balance = balance - pur.price
                })
                resolve([purchases])
            }
        })
    })
})

.then(function(args) {
    return new Promise(function (resolve, reject) {
        cust.getProfile(globalAccessToken, globalUserId, function(err, profile) {
            if (err) {
                reject(err)
            } else if (Math.abs(profile.balance - balance) > 0.1) {
                reject('Unexpected remaining balance: ' + profile.balance + '. Expected: ' + balance)
            } else {
                resolve([profile])
            }
        })
    })
})

.then(function(args) {
    return new Promise(function (resolve, reject) {
        auth.logout(globalAccessToken, function(err) {
            if (err) {
                reject(err)
            } else {
                resolve()
            }
        })
    })
})

.then(function(args) {
    return new Promise(function (resolve, reject) {
        auth.login(recipientLoginData, function(err, accessToken, userId) {
            if (err) {
                reject(err)
            } else {
                globalAccessToken = accessToken
                globalUserId = userId
                resolve([accessToken, userId])
            }
        })
    })
})

.then(function(args) {
    return new Promise(function (resolve, reject) {
        cust.downloadProduct(globalAccessToken, globalUserId, randomBundle.products[0].id, function(err, download) {
            if (err) {
                reject(err)
            } else {
                resolve([download])
            }
        })
    })
})

.then(function(args) {
    auth.logout(globalAccessToken, function(err) {
        if (err) throw err
        console.log('\x1b[32m%s\x1b[0m', 'Test success')
    })
})

.catch(function(err) {
    console.error('\x1b[31m%s\x1b[0m', err)
    auth.logout(globalAccessToken, function(err) {
        if (err) throw err
        console.warn('\x1b[31m%s\x1b[0m', 'Test failed')
    })
})

function getBundles(callback, filter) {
    var url = config.apiUrl + '/Bundles'
    if (filter) {
        url += '?filter=' + JSON.stringify(filter)
    }
    console.log('GET ' + url)
    request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
        body = body ? JSON.parse(body) : body
        if (callback) callback(err, body)
    })
}
