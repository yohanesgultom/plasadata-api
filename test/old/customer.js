// customer tests

var fs = require('fs'),
    request = require('request'),
    config = require('./config'),
    auth = require('./auth'),
    prod = require('./product'),
    headerData = config.headerData

/* Expose */

module.exports = {

    register: function (firstName, lastName, gender, email, username, password, callback) {
        var url = config.apiUrl + '/Customers',
            data = {
                firstName: firstName,
                lastName: lastName,
                gender: gender,
                email: email,
                username: username,
                password: password
            }
        console.log('POST ' + url)

        request.post({url: url, headers: headerData, body: JSON.stringify(data)}, function (err, httpResponse, body) {
            if (err) {
                // do nothing
            } else {
                var body = JSON.parse(body)
                err = body.error ? body.error : err
            }
            if (callback) callback(err, body)
        })
    },

    getProfile: function (accessToken, userId, callback) {
        var url = config.apiUrl + '/Customers/' + userId + '?access_token=' + accessToken
        console.log('GET ' + url)

        request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            if (err) {
                // do nothing
            } else {
                var body = JSON.parse(body)
                err = body.error ? body.error : err
            }
            if (callback) callback(err, body)
        })
    },

    // upload existing customer photo
    uploadPhoto: function (accessToken, userId, photo, callback) {
        var url = config.apiUrl + '/Customers/' + userId + '/photo-upload?access_token=' + accessToken
        console.log('POST ' + url)

        var formData = {
          photo: photo
        }

        request.post({url: url, headers: headerData, formData: formData}, function (err, httpResponse, body) {
            if (err) {
                // do nothing
            } else {
                var body = JSON.parse(body)
                err = body.error ? body.error : err
            }
            if (callback) callback(err, body)
        })
    },

    purchase: function (accessToken, userId, productId, callback) {
        var url = config.apiUrl + '/Customers/' + userId + '/purchase/' + productId + '?access_token=' + accessToken
        console.log('POST ' + url)
        request.post({url: url, headers: headerData}, function (err, httpResponse, body) {
            if (err) {
                // do nothing
            } else {
                var body = JSON.parse(body)
                err = body.error ? body.error : err
            }
            if (callback) callback(err, body)
        })
    },

    purchaseBundle: function (accessToken, userId, bundleId, callback) {
        var url = config.apiUrl + '/Customers/' + userId + '/purchase-bundle/' + bundleId + '?access_token=' + accessToken
        console.log('POST ' + url)
        request.post({url: url, headers: headerData}, function (err, httpResponse, body) {
            if (err) {
                // do nothing
            } else {
                var body = JSON.parse(body)
                err = body.error ? body.error : err
            }
            if (callback) callback(err, body)
        })
    },

    refund: function (accessToken, userId, purchaseIds, callback) {
        var url = config.apiUrl + '/Customers/' + userId + '/refund?access_token=' + accessToken,
            data = {
                purchases: purchaseIds
            }
        console.log('POST ' + url)
        request.post({url: url, headers: headerData, body: JSON.stringify(data)}, function (err, httpResponse, body) {
            if (err) {
                // do nothing
            } else {
                var body = JSON.parse(body)
                err = body.error ? body.error : err
            }
            if (callback) callback(err, body)
        })
    },

    getPurchasedProducts: function (accessToken, userId, callback) {
        var url = config.apiUrl + '/Customers/' + userId + '/products?access_token=' + accessToken
        console.log('GET ' + url)
        request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            if (err) {
                // do nothing
            } else {
                var body = JSON.parse(body)
                err = body.error ? body.error : err
            }
            if (callback) callback(err, body)
        })
    },

    downloadProduct: function (accessToken, userId, productId, callback) {
        var url = config.apiUrl + '/Customers/' + userId + '/products/' + productId + '/download?access_token=' + accessToken
        console.log('GET ' + url)
        request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            if (httpResponse.statusCode != 200) {
                body = JSON.parse(body)
                err = body.error ? body.error : err
            }
            if (callback) callback(err, body)
        })
    },

    downloadSubscriptionItem: function (accessToken, userId, productId, callback) {
        var url = config.apiUrl + '/Customers/' + userId + '/subscriptionItems/' + productId + '/download?access_token=' + accessToken
        console.log('GET ' + url)
        request.get({url: url, headers: headerData}, function (err, httpResponse, body) {
            if (httpResponse.statusCode != 200) {
                body = JSON.parse(body)
                err = body.error ? body.error : err
            }
            if (callback) callback(err, body)
        })
    },

    purchaseAsGift: function (accessToken, userId, recipientUsername, productId, callback) {
        var url = config.apiUrl + '/Customers/' + userId + '/purchase/' + productId + '/gift/' + recipientUsername + '?access_token=' + accessToken
        console.log('POST ' + url)
        request.post({url: url, headers: headerData}, function (err, httpResponse, body) {
            if (err) {
                // do nothing
            } else {
                var body = JSON.parse(body)
                err = body.error ? body.error : err
            }
            if (callback) callback(err, body)
        })
    },

    purchaseBundleAsGift: function (accessToken, userId, recipientUsername, bundleId, callback) {
        var url = config.apiUrl + '/Customers/' + userId + '/purchase-bundle/' + bundleId + '/gift/' + recipientUsername + '?access_token=' + accessToken
        console.log('POST ' + url)
        request.post({url: url, headers: headerData}, function (err, httpResponse, body) {
            if (err) {
                // do nothing
            } else {
                var body = JSON.parse(body)
                err = body.error ? body.error : err
            }
            if (callback) callback(err, body)
        })
    },

}
