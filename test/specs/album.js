var chai = require('chai'),
    should = chai.should(),
    chaiHttp = require('chai-http'),
    app

module.exports = (a, f) => {
    app = a
    resetDatabase = f
}

describe('Retrieving albums through REST API', () => {

    before('Clear database', (done) => {
        resetDatabase(app, () => {
            done()
        })
    })

    before(function(done) {
        app.models.Album.create({
            name: 'test album'
        }, (err, album) => {
            done()
        })
    })

    it('should be able to get albums', (done) => {
        chai.request(app)
            .get('/api/Albums')
            .end((err, res) => {
                res.status.should.equal(200)
                res.body.length.should.equal(1)
                res.body[0].name.should.equal('test album')
                res.body[0].createdAt.should.not.equal(null)
                done()
            })
    })
})

describe('Adding product to album', () => {

    it('should automatically set tags', (done) => {
        app.models.Album.create({
            name: 'test album'
        }, (err, album) => {
            app.models.Product.create(
                [
                    {
                        name: 'test product 1',
                        desc: 'test product 1 desc',
                        basePrice: 1,
                        tags: ['a', 'ignore']
                    },
                    {
                        name: 'test product 2',
                        desc: 'test product 2 desc',
                        basePrice: 1,
                        tags: ['b', 'ignore']
                    }
                ], (err, products) => {
                album.products.add(products[0], (err, product) => {
                    app.models.Album.findById(album.id, {include: 'products'}, (err, album) => {
                        album.products().length.should.equal(1)
                        album.tags.toObject().should.deep.equal(['a'])
                        album.products.add(products[1], (err, product) => {
                            app.models.Album.findById(album.id, {include: 'products'}, (err, album) => {
                                album.products().length.should.equal(2)
                                album.tags.toObject().should.deep.equal(['a', 'b'])
                                done()
                            })
                        })
                    })
                })
            })
        })
    })

    it('should be filterable by tag', (done) => {
        app.models.Album.create({
            name: 'test album filterable'
        }, (err, album) => {
            app.models.Product.create([{name: 'filterable product 1', tags: ['music', 'pop']}, {name: 'filterable product 2', tags: ['video', 'movie']}], (err, products) => {
                var promise = Promise.resolve()
                products.forEach((product) => {
                    promise = promise.then(() => {
                        return new Promise((resolve, reject) => {
                            album.products.add(product, (err, prod) => {
                                if (err) {
                                    reject(err)
                                } else {
                                    resolve(prod)
                                }
                            })
                        })
                    })
                })

                promise.then(() => {
                    chai.request(app)
                        .get('/api/Albums/' + album.id + '/products')
                        .query({filter: JSON.stringify({where: {tags: {inq: ['music']}}})})
                        .end((err, res) => {
                            res.status.should.equal(200)
                            res.body.length.should.equal(1)
                            done()
                        })
                })
            })
        })
    })
})

describe('Removing product from album', () => {

    it('should automatically update tags', (done) => {
        app.models.Album.create({
            name: 'test album'
        }, (err, album) => {
            app.models.Product.create([
                {
                    name: 'test product 3',
                    desc: 'test product 3 desc',
                    basePrice: 1,
                    tags: ['a', 'ignore']
                },
                {
                    name: 'test product 4',
                    desc: 'test product 4 desc',
                    basePrice: 1,
                    tags: ['b', 'ignore']
                },
                {
                    name: 'test product 5',
                    desc: 'test product 5 desc',
                    basePrice: 1,
                    tags: ['b', 'ignore']
                }
            ], (err, products) => {
                // add products sequentially
                products.reduce((p, prod) => {
                    return p.then(() => {
                        return new Promise((resolve) => {
                            album.products.add(prod, (err, pr) => { resolve(pr) })
                        })
                    })
                }, Promise.resolve())

                // remove products one by one
                .then((res) => {
                    return new Promise((resolve) => {
                        album.products.remove(products[2], (err) => {
                            should.not.exist(err)
                            app.models.Album.findById(album.id, {include: 'products'}, (err, al) => {
                                should.not.exist(err)
                                al.products().length.should.equal(2)
                                al.tags.toObject().should.deep.equal(['a', 'b'])
                                resolve(al)
                            })
                        })
                    })
                })
                .then((res) => {
                    return new Promise((resolve) => {
                        album.products.remove(products[1], (err) => {
                            should.not.exist(err)
                            app.models.Album.findById(album.id, {include: 'products'}, (err, al) => {
                                should.not.exist(err)
                                al.products().length.should.equal(1)
                                al.tags.toObject().should.deep.equal(['a'])
                                resolve(al)
                            })
                        })
                    })
                })
                .then((res) => {
                    return new Promise((resolve) => {
                        album.products.remove(products[0], (err) => {
                            // console.trace('here')
                            should.not.exist(err)
                            app.models.Album.findById(album.id, {include: 'products'}, (err, al) => {
                                should.not.exist(err)
                                al.products().length.should.equal(0)
                                al.tags.toObject().should.deep.equal([])
                                resolve(al)
                            })
                        })
                    })
                })
                .then((res) => {
                    done()
                })
            })
        })
    })

})

describe('Retrieving products through REST API', () => {

    var prodId

    before(function(done) {
        app.models.Album.create({
            name: 'test another album'
        }, (err, album) => {
            app.models.Product.create(
                {
                    name: 'product with album',
                    desc: 'product with album',
                    basePrice: 1,
                    tags: ['a', 'b']
                }
            , (err, product) => {
                album.products.add(product, (err, added) => {
                    prodId = added.id
                    done()
                })
            })
        })
    })

    it('should include albums', (done) => {
        chai.request(app)
            .get('/api/Products')
            .query({filter: {where:{name: 'product with album'}}})
            .end((err, res) => {
                res.status.should.equal(200)
                res.body.length.should.equal(1)
                res.body[0].name.should.equal('product with album')
                res.body[0].albums.should.not.be.empty
                done()
            })
    })

})
