var chai = require('chai'),
    should = chai.should(),
    chaiHttp = require('chai-http'),
    faker = require('faker'),
    app, resetDatabase, token, userId

module.exports = (a, f) => {
    app = a
    resetDatabase = f
}

describe('As a customer', () => {

    before('Clear database', (done) => {
        resetDatabase(app, () => {
            done()
        })
    })

    let customerData = {username: 'customertest', email: 'customertest@email.com', password: 'secret', emailVerified: true, balance: 300}

    before('Create customer data', (done) => {
        app.models.Role.create({name: 'buyer'}, (err, role) => {
            app.models.Customer.create(customerData, (err, newCustomer) => {
                userId = newCustomer.id
                createProducts(app).then((res) => {
                    done()
                })
            })
        })
    })

    it('should be able to login', (done) => {
        chai.request(app)
            .post('/api/Customers/login')
            .send(customerData)
            .end((err, res) => {
                res.status.should.equal(200)
                should.exist(res.body.id)
                token = res.body.id
                done()
            })
    })

    it('should be able to purchase music product', (done) => {
        app.models.Product.findOne({where: {tags: {inq: ['music']}}}, (err, product) => {
            chai.request(app)
                .post('/api/Customers/' + userId + '/purchase/' + product.id)
                .query({access_token: token})
                .end((err, res) => {
                    res.status.should.equal(200)
                    should.not.exist(err)
                    res.body.length.should.equal(1)
                    res.body[0].price.should.equal(100)
                    done()
                })
        })
    })

    it('should be able to purchase video product', (done) => {
        app.models.Product.findOne({where: {tags: {inq: ['video']}}}, (err, product) => {
            chai.request(app)
                .post('/api/Customers/' + userId + '/purchase/' + product.id)
                .query({access_token: token})
                .end((err, res) => {
                    res.status.should.equal(200)
                    should.not.exist(err)
                    res.body.length.should.equal(1)
                    res.body[0].price.should.equal(200)
                    done()
                })
        })
    })

    it('should have balance decreased', (done) => {
        chai.request(app)
            .get('/api/Customers/' + userId)
            .query({access_token: token})
            .end((err, res) => {
                res.status.should.equal(200)
                res.body.id.should.equal(userId)
                res.body.username.should.equal(customerData.username)
                res.body.balance.should.equal(0)
                done()
            })
    })

    it('should be able to see purchased products', (done) => {
        chai.request(app)
            .get('/api/Customers/' + userId + '/products')
            .query({access_token: token})
            .end((err, res) => {
                res.status.should.equal(200)
                res.body.length.should.equal(2)
                done()
            })
    })

    it('should be able to filter purchased products by music tag', (done) => {
        chai.request(app)
            .get('/api/Customers/' + userId + '/products')
            .query({access_token: token, filter: JSON.stringify({where: {tags: {inq: ['music']}}})})
            .end((err, res) => {
                res.status.should.equal(200)
                res.body.length.should.equal(1)
                res.body[0].tags.should.deep.equal(['music', 'pop'])
                done()
            })
    })

    it('should be able to logout', (done) => {
        chai.request(app)
            .post('/api/Customers/logout')
            .send({access_token: token})
            .end((err, res) => {
                res.status.should.equal(204)
                done()
            })
    })

})

/* Helper functions */

var createProducts = (app) => {
    var objects = [
        {
            name: faker.lorem.words(),
            tags: ['music', 'pop'],
            basePrice: 100
        },
        {
            name: faker.lorem.words(),
            tags: ['video', 'movie'],
            basePrice: 200
        }
    ]
    return new Promise((resolve, reject) => {
        app.models.Product.create(objects, (err, res) => {
            if (err) {
                reject(err)
            } else {
                resolve(res)
            }
        })
    })
}
