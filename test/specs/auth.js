var chai = require('chai'),
    should = chai.should(),
    chaiHttp = require('chai-http'),
    app, token, userId

module.exports = (a, f) => {
    app = a
    resetDatabase = f
}

describe('As a guest', () => {

    before('Clear database', (done) => {
        resetDatabase(app, () => {
            done()
        })
    })

    before('Create buyer role', (done) => {
        app.models.Role.create({name: 'buyer'}, (err, role) => {
            done()
        })
    })

    let data = {username: 'newuser', email: 'newuser@email.com', password: 'secret'}

    it('should be able to register', (done) => {
        chai.request(app)
            .post('/api/Customers')
            .send(data)
            .end((err, res) => {
                res.status.should.equal(200)
                res.body.username.should.equal(data.username)
                res.body.email.should.equal(data.email)
                userId = res.body.id
                done()
            })
    })

    it('should be able to login', (done) => {
        app.models.Customer.findOne({where: {username: data.username}}, (err, customer) => {
            customer.updateAttribute('emailVerified', true, (err, customer) => {
                chai.request(app)
                    .post('/api/Customers/login')
                    .send(data)
                    .end((err, res) => {
                        res.status.should.equal(200)
                        should.exist(res.body.id)
                        token = res.body.id
                        done()
                    })
            })
        })
    })

    it('should be able to get profile', (done) => {
        chai.request(app)
            .get('/api/Customers/' + userId)
            .query({access_token: token})
            .end((err, res) => {
                res.status.should.equal(200)
                res.body.id.should.equal(userId)
                res.body.username.should.equal(data.username)
                res.body.email.should.equal(data.email)
                res.body.username.should.equal(data.username)
                res.body.emailVerified.should.equal(true)
                done()
            })
    })

    it('should be able to logout', (done) => {
        chai.request(app)
            .post('/api/Customers/logout')
            .send({access_token: token})
            .end((err, res) => {
                res.status.should.equal(204)
                done()
            })
    })

})
