# Plasadata API server

Plasadata API server powered by Loopback https://loopback.io/

### Prerequisites

* Node & NPM https://nodejs.org
* PostgreSQL https://www.postgresql.org/
* MongoDB https://www.mongodb.com/
* Build essentials (debian/ubuntu: `apt-get install -y build-essential`)

### Installation (dev)

1. Create database `plasadata` in PostgreSQL and MongoDB
1. Clone this repo
1. Create datasource in `server/datasources.json` using `server/datasources.example.json` as template
1. Create `server/config.json` using `server/config.example.json` as template
1. Make sure PostgreSQL and MongoDB are up
1. Enter the repo and run:
```
$ npm install -g strongloop
$ npm install
$ node .
```
1. (Optional) Create database constraints (use absolute path with this script)
```
$ sudo -i -u postgres psql -d plasadata -a -f /absolute/path/plasadata-api/postgresql/create-constraints.sql
```
1. Explore the APIs in http://localhost:3000/explorer/

1. Explore the model diagram at http://localhost:3000/model-diagram/

### Configuration

Configuration file that needs to be modified on production:
* Veritrans payment API: `server/config.json`
* Database config: `server/datasources.json`
* SMTP Server config: `server/datasources.json`
* File (upload) storage: `server/boot/00-config-storage.js`
* Initial app data (users & roles): `server/boot/01-init-app-data.js`
* Scheduler: `server/boot/02-init-scheduler.js`
* Test data (**must** be removed on production): `server/boot/99-test-data.js`

### Running API test

> Warning: some tests are still changing the database (adding customer and purchases)

Bring up the API and run the test script individually:

```
$ node test/productTest.js
```
or run all of them:

```
$ ./run_api_test
```

against specific server address:

```
$ ./run_api_test http://localhost:3001
```


### Documentations

**Generic Rest API**

General documentation for querying resourcefully can be found in https://docs.strongloop.com/display/public/LB/Querying+data

Generic API pattern to filter result:

```
GET /Model?filter={ Stringified-JSON }
```

Example of filtering by tags:
```
GET /Products?filter={"where": {"tags": {"inq": ["ebook"]}}}
```

Example of search by name (regex):
```
GET /Products?filter={"where": {"name": {"regexp": "/.*Great.*/i"}}}
```

Example of pagination (using count, offset and limit):
```
GET /Products/count?where={"where": {"tags": {"inq": ["music"]}}}

GET /Products?filter={"where": {"tags": {"inq": ["music"]}}, "offset":1, "limit": 2}
```

To get top selling products:
```
GET /Products/top-selling?tag={tag}&limit={limit}offset={offset}
```

To get promoted products with pagination:
```
GET /Products/promoted/count?tag={tag}

GET /Products/promoted?tag={tag}&limit={limit}offset={offset}
```

Getting related products (other products with same tag):
```
GET /Products?filter={"where":{"tags":{"inq":["tag"]}, "id":{"nin":["productId"]}}}
```

Application specific API documentation can be found in Swagger Explorer `/explorer` when the server is running

> Note: the actual URL will have JSON syntax encoded

**Promotion API**

Generic API to get promotions and promotion detail:
```
GET /Promotions

GET /Promotions/{id}
```

To get products with their promotions (can be empty):
```
GET /Products?filter={"include":["promotions"]}

GET /api/Products/{productId}?filter={"include":["promotions"]}
```

**Bundle API**

To get list of Bundles:
```
GET /Bundles
```

To get list of bundles where a product included:
```
GET /Products/{id}/?filter={"include":["bundles"]}
```

**Purchasing API**

In order to purchase products, customer needs to login first and needs to include valid `customerId` and `access_token` in every API calls.

To purchase single product/subscription directly:
```
POST /Customers/{customerId}/purchase/{productId}
```

Purchasing bundle directly:
```
POST /Customers/{customerId}/purchase-bundle/{bundleId}
```

Purchasing using cart:
```
PUT /Customers/{customerId}/cart body: {"products": ["productId1", "productId2"], "bundles": ["bundleId1","bundleId2"]}

POST /Customers/{customerId}/validate

POST /Customers/{customerId}/checkout
```

**Purchasing as GIFT API**

Gift purchasing flows are absolutely the same as normal purchasing, except using different purchase API.

To purchase single product/subscription as gift:
```
POST /Customers/{customerId}/purchase/{productId}/gift/{recepientUsername}
```

Purchasing bundle as gift:
```
POST /Customers/{customerId}/purchase-bundle/{bundleId}/gift/{recepientUsername}
```

Purchasing gift using cart:
```
POST /Customers/{customerId}/checkout-gift/{recepientUsername}
```

**Veritrans Snap API (Top-up) integration**

`POST /Customers/{customerId}/topup`

Get Veritrans Snap API configuration (veritransSnapToken & veritransClientKey) to trigger web-based payment https://snap-docs.midtrans.com/#frontend-integration. OrderId is generated id (for status check) and amount is the topup amount passed in request (for confirmation only)
```
url: POST /Customers/{customerId}/topup
request: {amount: number}
response: {
    veritransSnapToken: string,
    veritransClientKey: string,
    orderId: string,
    amount: number,
}
```

`GET /Customer/{customerId}/topup-status/{orderId}` (backup/optional)

Check payment status and a fail-over to trigger customer balance's update (in case automatic balance update not working)
```
url: GET /Customers/{customerId}/topup-status/{orderId}
response: {
  "value": 2000000,
  "code": "TOPUP",
  "info": "orderId",
  "date": "2017-01-03T13:05:39.201Z",
  "id": "586ba1a365cfff11e35b84e0",
  "customerId": 2
}
```

Backend APIs (configured on and will be triggered by veritrans server):

```
GET /Payments/finish?order_id=string
GET /Payments/unfinish?order_id=string
GET /Payments/error?order_id=string
POST /Payments/notification
```

**Third Party Login**

Login from mobile app with facebook:

```
url: POST /Customers/third-party-login
request: {provider: 'facebook', user_access_token: string, user_id: string, email: string}
response: {
  "id": "string",
  "ttl": -1,
  "created": "2017-03-01",
  "userId": 0
}
```
Get `user_access_token`, `user_id` & `email` using facebook SDK (prompting user permission). Currently `email` is optional (but suggested) because facebook users don't always have one.
